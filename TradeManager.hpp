//
//  TradeManager.hpp
//  
//
//  Created by Tim Cabble on 11/03/2015.
//
//

#ifndef TradeManager_hpp
#define TradeManager_hpp


#include "Indi.hpp"
#include "Evaluator.hpp"
#include "Trade.hpp"
#include "Ledger.hpp"

class Evaluator;

class TradeManager {

public:
	TradeManager(Evaluator *eval, Indi *indiv, int startIndex, int stopIndex);
	virtual ~TradeManager();
    
    int currentIndex;
    
    int stopIndex;
    
    Evaluator* getEvaluator();
    
    Indi* getIndividual();
    
    void runTrades(int mode);
    
    Trade getCurrentTrade();

private:
	Evaluator *eval;
    Indi *indiv;
    
	//int currentIndex;
	int dir;

	float currentTradeDrawdown;
	float currentTradeReturn;
	float maxDrawdown;
	float totalReturn;
	Trade currentTrade;
    
    void manageTrade(int mode);
    void closeTrade(Bar curBar, int mode);

	void step();
    void newTrade();
    void setTrade(Trade newTrade);
    

};

#endif /* TradeManager_hpp */












