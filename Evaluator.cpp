//
//  Evaluator.cpp
//  
//
//  Created by Tim Cabble on 10/26/15.
//
//

#include "Evaluator.hpp"

using namespace std::placeholders;

Evaluator::Evaluator(ChartData *historyData, int currentIndex){
    this->PC = 0;
    this->dataStore = historyData;
    this->currentDataIndex = currentIndex;
}

Evaluator::~Evaluator() {
    
}

void Evaluator::reset(int index, Indi *indiv, Trade *newTrade, int mode) {
    this->PC = 0;
    this->currentDataIndex = index;
    this->indiv = indiv;
    this->workingTrade = newTrade;
    this->setDealSize(mode);

}

void Evaluator::setDealSize(int mode) {
    this->workingTrade->setPositionSize(this->indiv->getLedgers(mode)->getDealSize());
}

void Evaluator::setCurrentIndex(int index) {
    this->currentDataIndex = index;
}

void Evaluator::step() {
    this->currentDataIndex--;
    this->PC = 0;
}

Bar Evaluator::getCurrentBar() {
    return this->dataStore->getBar(this->currentDataIndex);
}

Bar Evaluator::getBarAt(int index) {
    return this->dataStore->getBar(index);
}

/*
Indi* Evaluator::getIndividual() {
    return this->indiv;
}

void Evaluator::setIndividual(Indi *indiv) {
    this->indiv = indiv;
}
*/

float Evaluator::run() {
    std::string program = this->indiv->program;
    //std::cout << "Run" << std::endl;

    char node = program[this->PC];

    //std::cout << (int)node << std::endl;
    
    this->PC++;

    int decision = 0;
    int then = 0;
    int elseThen = 0;
    int arg1 = 0, arg2 = 0, arg3 = 0, arg4 = 0, arg5 = 0, indicatorIndex = 0;
    std::function<float(int&, int&, int&, int&, int&)> indicator;
    int a, b;
    double intpart;
    float fracpart;
    float tempResult = 0.0;
    
    //std::cout << PC << std::endl;
    if (node < 90 ){//this->test->FSET_START
        //std::cout << "Returning: " << (int)node << std::endl;
        return ((int)node);
    }
    switch(node){
         case 99:
            //std::cout<<"HEAD ";
            decision = run();
            //std::cout<<"Should go LONG ";
            run();
            //std::cout<<"Should go SHORT ";
            run();
            return decision;
        case 100:
            //std::cout<<"IFELSE ";
            if (run() == 1) {
                then = run();
                run();
                return then;
            } else {
                run();
                elseThen = run();
                return elseThen;
            }
        case 101:
            //std::cout<<"AND ";
            a = run();
            b = run();
            if (a == 1 && b == 1) {
                return 1;
            } else {
                return -1;
            }
        case 102:
            //std::cout<<"OR ";
            a = run();
            b = run();
            if (a == 1 || b == 1) {
                return 1;
            } else {
                return -1;
            }
        case 103:
            //std::cout<<"XOR ";
            a = run();
            b = run();
            if ((a == 1 && b == -1) || (a == -1 && b == 1)) {
                return 1;
            } else {
                return -1;
            }
        case 104:
            //std::cout<<"LESS ";
            a = run();
            b = run();
            if (a < b) {
                return 1;
            } else {
                return -1;
            }
        case 105:
            //std::cout<<"GREAT ";
            a = run();
            b = run();
             if (a > b) {
                return 1;
            } else {
                return -1;
            }
        case 106:
            //std::cout<<"EQU ";
            a = run();
            b = run();
             if (a == b) {
                return 1;
            } else {
                return -1;
            }
        case 107:
            //std::cout<<"ADD ";
            a = run();
            b = run();
            return(a + b);
        case 108:
            //std::cout<<"SUB ";
            a = run();
            b = run();
            return(a - b);
        case 109:
            //std::cout<<"MUL ";
            return(run() * run());
        case 110:
            //std::cout<<"DIV ";
            {
            float num = run();
            float den = run();
            if (std::fabs(den)< 0.0000001) return (-1.0); //protected division
            else return (num/den);
            }
        case 111:
            //std::cout<<"MIN ";
            return(std::min(run(), run()));
        case 112:
            //std::cout<<"MAX ";
            return(std::max(run(), run()));
        case 113:
            //std::cout<<"NEG ";
            return -(run());
        case 114:
            //std::cout<<"SIN ";
            return (sin(run()));
        case 115:
            //std::cout<<"COS ";
            return (cos(run()));
        case 116:
            //std::cout<<"TAN ";
            return (tan(run()));
        case 117:
            //std::cout<<"LOG ";
            return (log(std::fabs(run())));
        case 118:
            //std::cout<<"MA ";
            indicatorIndex = this->getIndicatorIndex(0);
            //std::cout<<" " << indicatorIndex << " ";
            arg1 = run();
            arg2 = run();
            arg3 = run();
            arg4 = run() + this->currentDataIndex;
            arg5 = run();
            indicator = this->getIndicator(indicatorIndex);
            tempResult = indicator(arg1, arg2, arg3, arg4, arg5);
            //std::cout << " " << tempResult << " ";
            return tempResult;
            //return indicator(arg1, arg2, arg3, arg4, arg5);
            /*std::cout<<"MA ";
            arg1 = run();
            arg2 = run();
            arg3 = run();
            return this->ma(arg1, arg2, arg3);
            */
        case 119:
            //std::cout<<"MACD ";
            indicatorIndex = this->getIndicatorIndex(1);
            //std::cout<<" " << indicatorIndex << " ";
            arg1 = run();
            arg2 = run();
            arg3 = run();
            arg4 = run() + this->currentDataIndex;
            arg5 = run();
            indicator = this->getIndicator(indicatorIndex);
            tempResult = indicator(arg1, arg2, arg3, arg4, arg5);
            //std::cout << " " << tempResult << " ";
            return tempResult;
            //return indicator(arg1, arg2, arg3, arg4, arg5);
            /*std::cout<<"MACD ";
            arg1 = run();
            arg2 = run();
            arg3 = run();
            arg4 = run();
            arg5 = run();
            return this->macd(arg1, arg2, arg3, arg4, arg5);
             */
        case 120:
            //std::cout<<"STOCH ";
            indicatorIndex = this->getIndicatorIndex(2);
            //std::cout<<" " << indicatorIndex << " ";
            arg1 = run();
            arg2 = run();
            arg3 = run();
            arg4 = run() + this->currentDataIndex;
            arg5 = run();
            indicator = this->getIndicator(indicatorIndex);
            tempResult = indicator(arg1, arg2, arg3, arg4, arg5);
            //std::cout << " " << tempResult << " ";
            return tempResult;
            //return indicator(arg1, arg2, arg3, arg4, arg5);
            /*std::cout<<"STOCH ";
            arg1 = run();
            arg2 = run();
            arg3 = run();
            arg4 = run();
            arg5 = run();
            return this->stoch(arg1, arg2, arg3, arg4, arg5);
            */
        case 121:
            //std::cout<<"NEXT ";
            indicatorIndex = this->getIndicatorIndex(3);
            //std::cout<<" " << indicatorIndex << " ";
            arg1 = run();
            arg2 = run();
            arg3 = run();
            arg4 = run() + this->currentDataIndex;
            arg5 = run();
            indicator = this->getIndicator(indicatorIndex);
            tempResult = indicator(arg1, arg2, arg3, arg4, arg5);
            //std::cout << " " << tempResult << " ";
            return tempResult;
            //return indicator(arg1, arg2, arg3, arg4, arg5);
            /*std::cout<<"RSI ";
            arg1 = run();
            arg2 = run();
            return this->rsi(arg1, arg2);
            */
        case 122:
            //std::cout<<"LAST ";
            indicatorIndex = this->getIndicatorIndex(4);
            //std::cout<<" " << indicatorIndex << " ";
            arg1 = run();
            arg2 = run();
            arg3 = run();
            arg4 = run() + this->currentDataIndex;
            arg5 = run();
            indicator = this->getIndicator(indicatorIndex);
            tempResult = indicator(arg1, arg2, arg3, arg4, arg5);
            //std::cout << " " << tempResult << " ";
            return tempResult;
            //return indicator(arg1, arg2, arg3, arg4, arg5);
            /*std::cout<<"MOM ";
            arg1 = run();
            arg2 = run();
            return this->mom(arg1, arg2);
            */
        case 123:
            //std::cout<<"OPEN ";
            return this->open(run());
        case 124:
            //std::cout<<"HIGH ";
            return this->high(run());
        case 125:
            //std::cout<<"LOW ";
            return this->low(run());
        case 126:
            //std::cout<<"CLOSE ";
            return this->close(run());
        case 127:
            //std::cout<<"VOLUME ";
            return this->volume(run());
        case 90:
            //std::cout<<"--LONG-- ";
            fracpart = run();
            //std::cout << "SL: " << fracpart << " ";
            this->workingTrade->setLongSL(fracpart, this->getBarAt(this->currentDataIndex - 1));//this->getBarAt(this->currentDataIndex - 1)
            fracpart = run();
            //std::cout << "TP: " << fracpart << " ";
            this->workingTrade->setLongTP(fracpart, this->getBarAt(this->currentDataIndex - 1));//this->getCurrentBar()
            return 1;
        case 91:
            //std::cout<<"--SHORT-- ";
            fracpart = run();
            //std::cout << "SL: " << fracpart << " ";
            this->workingTrade->setShortSL(fracpart, this->getBarAt(this->currentDataIndex - 1));//this->getCurrentBar()
            fracpart = run();
            //std::cout << "TP: " << fracpart << " ";
            this->workingTrade->setShortTP(fracpart, this->getBarAt(this->currentDataIndex - 1));//this->getCurrentBar()
            return 1;
        case 92:
            //std::cout<<"SL ";
            fracpart = modf(run(), &intpart);
            fracpart = std::fabs(fracpart) * this->dataStore->getAverageRange() * 10000;
            return fracpart;
            //return std::fabs(run());
        case 93:
            //std::cout<<"TP ";
            fracpart = modf(run(), &intpart);
            fracpart = std::fabs(fracpart) * this->dataStore->getAverageRange() * 10000;
            return fracpart;
            //return std::fabs(run());
    }
    return (0);
}

void Evaluator::test() {
    

    
}

float Evaluator::sma(int &period, int &two, int &three, int &shift, int &opt_line) {
    
    int paddedPeriod = period + 100;
    
    TA_Real closePrice[paddedPeriod];
    TA_Real out;
    TA_Integer outBeg;
    TA_Integer outNbElement;
    
    this->dataStore->getBarsArray(shift, paddedPeriod, closePrice, 3);
/*
    for (int i = 0; i < paddedPeriod; i++) {
        std::cout << "Index " << i << " Value " << closePrice[i] << std::endl;
    }
*/
    TA_RetCode retCode;
    
    retCode = TA_MA(paddedPeriod - 1, paddedPeriod - 1, closePrice, period, TA_MAType_SMA, &outBeg, &outNbElement, &out);
    
    return out;
}

float Evaluator::macd(int &fast, int &slow, int &sig, int &shift, int &opt_line) {
    int paddedPeriod = slow + 100;
    
    TA_Real closePrice[paddedPeriod];
    TA_Real out;
    TA_Real outSig;
    TA_Real outHist;
    TA_Integer outBeg;
    TA_Integer outNbElement;
    
    this->dataStore->getBarsArray(shift, paddedPeriod, closePrice, 3);
/*
    for (int i = 0; i < paddedPeriod; i++) {
        std::cout << "Index " << i << " Value " << closePrice[i] << std::endl;
    }
*/
    TA_RetCode retCode;
    
    retCode = TA_MACD(paddedPeriod - 1, paddedPeriod - 1, closePrice, fast, slow, sig, &outBeg, &outNbElement, &out, &outSig, &outHist);
    
    switch (opt_line) {
        case 0:
            return out;
        case 1:
            return outSig;
        case 2:
            return outHist;
        default:
            return out;
    }
}

float Evaluator::stoch(int &K, int &smooth, int &D, int &shift, int &opt_line) {
    int paddedPeriod = K + 100;
    
    TA_Real highPrice[paddedPeriod];
    TA_Real lowPrice[paddedPeriod];
    TA_Real closePrice[paddedPeriod];
    TA_Real outK;
    TA_Real outD;
    TA_Integer outBeg;
    TA_Integer outNbElement;
    
    this->dataStore->getBarsArray(shift, paddedPeriod, highPrice, 1);
    this->dataStore->getBarsArray(shift, paddedPeriod, lowPrice, 2);
    this->dataStore->getBarsArray(shift, paddedPeriod, closePrice, 3);
/*
    for (int i = 0; i < paddedPeriod; i++) {
        std::cout << "Index " << i << " Value " << closePrice[i] << std::endl;
    }
*/
    TA_RetCode retCode;
    
    retCode = TA_STOCH(paddedPeriod - 1, paddedPeriod - 1, highPrice, lowPrice, closePrice, K, smooth, TA_MAType_EMA, D, TA_MAType_EMA, &outBeg, &outNbElement, &outK, &outD);
    
    switch (opt_line) {
        case 0:
            return outK;
        case 1:
            return outD;
        default:
            return outK;
    }
}

float Evaluator::rsi(int &period, int &two, int &three, int &shift, int &opt_line) {
    
    int paddedPeriod = period + 100;
    
    TA_Real closePrice[paddedPeriod];
    TA_Real out;
    TA_Integer outBeg;
    TA_Integer outNbElement;
    
    this->dataStore->getBarsArray(shift, paddedPeriod, closePrice, 3);
/*
    for (int i = 0; i < paddedPeriod; i++) {
        std::cout << "Index " << i << " Value " << closePrice[i] << std::endl;
    }
*/    
    TA_RetCode retCode;

    retCode = TA_RSI(paddedPeriod - 1, paddedPeriod - 1, closePrice, period,  &outBeg, &outNbElement, &out);
    
    return out;
}

float Evaluator::mom(int &period, int &two, int &three, int &shift, int &opt_line) {

    int paddedPeriod = period + 100;
    
    TA_Real closePrice[paddedPeriod];
    TA_Real out;
    TA_Integer outBeg;
    TA_Integer outNbElement;
    
    this->dataStore->getBarsArray(shift, paddedPeriod, closePrice, 3);
/*
    for (int i = 0; i < paddedPeriod; i++) {
        std::cout << "Index " << i << " Value " << closePrice[i] << std::endl;
    }
*/
    TA_RetCode retCode;
    
    retCode = TA_MOM(paddedPeriod - 1, paddedPeriod - 1, closePrice, period,  &outBeg, &outNbElement, &out);
    return out;
}

float Evaluator::kama(int &period, int &two, int &three, int &shift, int &opt_line) {
    
    
    int paddedPeriod = period + 100;
    
    TA_Real closePrice[paddedPeriod];
    TA_Real out;
    TA_Integer outBeg;
    TA_Integer outNbElement;
    
    this->dataStore->getBarsArray(shift, paddedPeriod, closePrice, 3);
 /*
    for (int i = 0; i < paddedPeriod; i++) {
        std::cout << "Index " << i << " Value " << closePrice[i] << std::endl;
    }
*/
    TA_RetCode retCode;
    
    retCode = TA_KAMA(paddedPeriod - 1, paddedPeriod - 1, closePrice, period,  &outBeg, &outNbElement, &out);
    
    return out;
}

float Evaluator::roc(int &period, int &two, int &three, int &shift, int &opt_line) {
    
    int paddedPeriod = period + 100;
    
    TA_Real closePrice[paddedPeriod];
    TA_Real out;
    TA_Integer outBeg;
    TA_Integer outNbElement;
    
    this->dataStore->getBarsArray(shift, paddedPeriod, closePrice, 3);
/*
    for (int i = 0; i < paddedPeriod; i++) {
        std::cout << "Index " << i << " Value " << closePrice[i] << std::endl;
    }
*/
    TA_RetCode retCode;
    
    retCode = TA_ROC(paddedPeriod - 1, paddedPeriod - 1, closePrice, period,  &outBeg, &outNbElement, &out);
    
    return out;
}

float Evaluator::open(int index) {
    return this->dataStore->getBar(this->currentDataIndex + index).OPEN;
}

float Evaluator::high(int index) {
    return this->dataStore->getBar(this->currentDataIndex + index).HIGH;
}

float Evaluator::low(int index) {
    return this->dataStore->getBar(this->currentDataIndex + index).LOW;
}

float Evaluator::close(int index) {
    return this->dataStore->getBar(this->currentDataIndex + index).CLOSE;
}

float Evaluator::volume(int index) {
    return this->dataStore->getBar(this->currentDataIndex + index).VOLUME;
}

int Evaluator::getIndicatorIndex(int index) {
    return this->indicatorIndex.at(index);
}

void Evaluator::addIndicator(int masterIndex) {
    this->indicatorIndex.push_back(masterIndex);
}

std::function<float(int&, int&, int&, int&, int&)> Evaluator::getIndicator(int index) {
    switch (index) {
        case 0:
            return std::bind(&Evaluator::sma, this, _1, _2, _3, _4, _5);
        case 1:
            return std::bind(&Evaluator::macd, this, _1, _2, _3, _4, _5);
        case 2:
            return std::bind(&Evaluator::stoch, this, _1, _2, _3, _4, _5);
        case 3:
            return std::bind(&Evaluator::rsi, this, _1, _2, _3, _4, _5);
        case 4:
            return std::bind(&Evaluator::mom, this, _1, _2, _3, _4, _5);
        case 5:
            return std::bind(&Evaluator::kama, this, _1, _2, _3, _4, _5);
        case 6:
            return std::bind(&Evaluator::roc, this, _1, _2, _3, _4, _5);
    }
    return 0;
}

float Evaluator::ta_libTest(int &period, int &shift, int &three, int &four, int &five) {
    int paddedPeriod = period + 100;
    
    TA_Real closePrice[paddedPeriod];
    TA_Real out;
    TA_Real outSig;
    TA_Real outHist;
    TA_Integer outBeg;
    TA_Integer outNbElement;
    
    this->dataStore->getBarsArray(shift, paddedPeriod, closePrice, 3);

    for (int i = 0; i < paddedPeriod; i++) {
        std::cout << "Index " << i << " Value " << closePrice[i] << std::endl;
    }

    TA_RetCode retCode;
    
    retCode = TA_MA(paddedPeriod - 1, paddedPeriod - 1, closePrice, period, TA_MAType_EMA, &outBeg, &outNbElement, &out);
    
    //retCode = TA_MACD(paddedPeriod - 1, paddedPeriod - 1, closePrice, 12, 26, 9, &outBeg, &outNbElement, &out, &outSig, &outHist);
    
    //std::cout << "Vals: " << out << " : " << outSig << " : " << outHist << std::endl;
    
    return out;
}












