//
//  codeTester.cpp
//  
//
//  Created by Tim Cabble on 1/20/16.
//
//

#include <time.h>
#include <thread>
#include <mutex>
#include <random>
#include <chrono>
#include "Run.hpp"
#include "GPTest.hpp"
#include "Engine.hpp"
#include "Evaluator.hpp"
#include "Trade.hpp"
#include "TradeManager.hpp"



void launch(ChartData dataStore, Engine *theEngine, int seed, int index);

void testThread(int index);

int main(int argc, char *argv[]){
    ChartData dataStore;
    char *filename = argv[1];
    dataStore.loadData(filename);
    
    
    
#define NUM_DEMES 1
    std::thread t[NUM_DEMES];
    
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, 36500);
    
    std::vector<std::vector<std::pair<int, int>> > ranges;
    
    for (int i = 0; i< NUM_DEMES; ++i) {
        std::vector<std::pair<int, int> > newPairVector;
        ranges.push_back(newPairVector);
    }
    ranges.at(0).emplace_back(std::make_pair(10000, 9700));
    ranges.at(0).emplace_back(std::make_pair(9699, 9400));
    //ranges.at(1).emplace_back(std::make_pair(9399, 9100));
    //ranges.at(1).emplace_back(std::make_pair(8099, 8800));
    //ranges.at(2).emplace_back(std::make_pair(8799, 8500));
    //ranges.at(2).emplace_back(std::make_pair(8499, 8200));
    //ranges.at(3).emplace_back(std::make_pair(8199, 7900));
    //ranges.at(3).emplace_back(std::make_pair(7899, 7600));
    
    
    Engine theEngine(NUM_DEMES);
    
    for (int i = 0; i < NUM_DEMES; ++i) {
        std::cout << "Demes setup" << std::endl;
        Deme theDeme;
        theDeme.is_done = false;
        theDeme.trainingRange = ranges.at(i).at(0);
        theDeme.validationRange = ranges.at(i).at(1);
        theEngine.addDemeStruct(theDeme);
        std::cout << "Size is " << theEngine.getDemeStructs().size() << std::endl;
        std::cout << "Pushed" << std::endl;

    }
    
    std::cout << "Demes setup done" << std::endl;
    
    
    for (int i = 0; i < NUM_DEMES; ++i) {
        int seed = dis(gen);
        
        //t[i] = std::thread(testThread, i);
        t[i] = std::thread(launch, dataStore, &theEngine, seed, i);
    }
    
    for (int i = 0; i < NUM_DEMES; ++i) {
        t[i].join();
    }
 
    
    std::cout << "Done!" << std::endl;
    
    
}

void testThread(int index) {
    while (1) {
        std::stringstream stream;
        stream << "------------------- Thread: " << index << " -----------------";
        std::cout << stream.str() << std::endl;
    }
}

void launch(ChartData dataStore, Engine *theEngine, int seed, int index) {
    std::cout << "launching Thread" << std::endl;
    Deme theDeme = theEngine->getDemeStruct(index);
    
    Evaluator evaluator(&dataStore, 0);
    
    evaluator.addIndicator(0);
    evaluator.addIndicator(1);
    evaluator.addIndicator(2);
    evaluator.addIndicator(5);
    evaluator.addIndicator(3);
    
    FitnessFunction theFunction;
    
    GPTest theTest(theEngine, &theDeme, &theFunction, &evaluator, seed);

    theTest.gens = 0;
    
    Run newRun(&theTest, index);
    
    theEngine->addDemeTest(&theTest);
    
    newRun.createRandomPopulation();

    newRun.population.at(1).printIndividual();
    std::cout << std::endl;
    newRun.population.at(1).codeIndividual(0);
    
    theEngine->demeIsDone(index);
}
