#ifndef Engine_hpp
#define Engine_hpp

#include <stdio.h>
#include <mutex>
#include <vector>
#include <sstream>
#include <iostream>
#include <cmath>
#include <utility>
#include <algorithm>
#include <queue>
#include "Indi.hpp"
#include "GPTest.hpp"

struct Deme {
    bool is_done;
    bool ready_to_run = false;
    int gens_without_imp;
    int populationSize;
	int maxLength;
	int minLength;
	int maxDepth;
	float crossover_prob;
    float migration_prob;
	int tourn_size;
	std::pair <int, int> trainingRange;
    std::pair <int, int> validationRange;
    std::pair <int, int> testingRange;
};

class GPTest;

class Engine {


public:
	Engine(int numDemes);
	~Engine();
    
    static std::mutex mtx;
    
    int numDemes;

	Deme getDemeStruct(int index);
    
    void demeIsDone(int index);
    
    void demeIsReady(int index);
    
    bool allDemesReady();
    
    void addDemeStruct(Deme theDeme);

	void enqueueIndividual(int index, Indi indiv);
	std::vector<Indi> migrateIndividuals(int index);
    
    std::vector<std::queue<Indi> > getTravelers();

	void addDemeTest(GPTest *deme);
    
    std::vector<Deme> getDemeStructs();
    
    void addIndividual(Indi indiv);
    Indi getIndividual(int index);
    void sortIndividuals();
    
    void printResults();
    void printCode(int index);


	void setGensWithout(int index, int value);
	void setPopulationSize(int index, int value);
	void setMaxLength(int index, int value);
	void setMinLength(int index, int value);
	void setMaxDepth(int index, int value);
	void setCrossoverProb(int index, int value);
	void setTournSize(int index, int value);
	void setTrainingRange(int index, std::pair <int, int>);
	void setValidationRange(int index, std::pair <int, int>);
	void setTestingRange(int index, std::pair <int, int>);


private:
    
	std::vector<Deme> theDemes;
	std::vector<std::queue<Indi> > travelers;
	std::vector<GPTest*> demeRuns;
	std::vector<Indi> bestIndividuals;

};


#endif /* Engine_hpp */