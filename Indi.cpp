/*
 * Indi.cpp
 *
 *  Created on: Feb 11, 2014
 *      Author: Tim
 */

#include "Indi.hpp"
#include <iostream>
#include <vector>
#include <stdlib.h>

//constructor with pointer to the test instance of the test
Indi::Indi(){
	fitness = 0.0;
	equity = 0.0;
    PC = 0;
    this->myLedgers.resize(3);
}

Indi::~Indi() {
	//delete test;
}

void Indi::setLedger(Ledger &aLedger, int index){
    this->myLedgers.at(index) = aLedger;
}

Ledger* Indi::getLedgers(int index) {
    return &this->myLedgers.at(index);
}

//method to start the recursive printing of the individual
void Indi::printIndividual(){
    std::cout << "Printing Indi" << std::endl;
	recPrintIndividual(this->program, 0);
}

//the recursive function for traversal and print
int Indi::recPrintIndividual(std::string &tempProgram, int bufferCnt){
    //std::cout << tempProgram.size() << std::endl;
    
	int a1 = 0,a2 = 0, a3 = 0, a4 = 0, a5 = 0;
	if ((int)tempProgram[bufferCnt] < 90){//this->test->FSET_START){
		
        std::cout<< "val=" << (int)tempProgram[bufferCnt++] << ",";
		
		return(bufferCnt);
	}
    //std::cout << "In Here" << std::endl;
	switch((int)tempProgram[bufferCnt]){
        case 99:
            std::cout<<"HEAD ";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 100:
            std::cout<<"IFELSE ";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 101:
            std::cout<<"AND";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 102:
            std::cout<<"OR";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 103:
            std::cout<<"XOR";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 104:
            std::cout<<"LESS";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 105:
            std::cout<<"GREAT";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 106:
            std::cout<<"EQU";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 107:
            std::cout<<"ADD";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 108:
            std::cout<<"SUB";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 109:
            std::cout<<"MUL";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 110:
            std::cout<<"DIV";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 111:
            std::cout<<"MIN";
           std::cout << "(";
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           
           break;
        case 112:
            std::cout<<"MAX";
           std::cout << "(";
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           
           break;
        case 113:
            std::cout<<"NEG";
           std::cout << "(";
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           
           break;
        case 114:
            std::cout<<"SIN";
           std::cout << "(";
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 115:
            std::cout<<"COS";
           std::cout << "(";
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 116:
            std::cout<<"TAN";
           std::cout << "(";
           
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 117:
            std::cout<<"LOG";
           std::cout << "(";
           
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 118:
            std::cout<<"MA";
           std::cout << "(";
           
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 119:
            std::cout<<"MACD";
           std::cout << "(";
           
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 120:
            std::cout<<"STOCH";
           std::cout << "(";
           
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 121:
            std::cout<<"RSI";
           std::cout << "(";
           
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 122:
            std::cout<<"MOM";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 123:
            std::cout<<"OPEN";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 124:
            std::cout<<"HIGH";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 125:
            std::cout<<"LOW";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 126:
            std::cout<<"CLOSE";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 127:
            std::cout<<"VOLUME";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 90:
            std::cout<<"LONG";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 91:
            std::cout<<"SHORT";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 92:
            std::cout<<"SL";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 93:
            std::cout<<"TP";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;


        }
        if((int)tempProgram[bufferCnt-1] > 100 && (int)tempProgram[bufferCnt-1] < 113) {
          a2=recPrintIndividual(tempProgram,a1);
          std::cout<<")";
          return(a2);
        } else if ((int)tempProgram[bufferCnt-1] > 129 && (int)tempProgram[bufferCnt-1] < 132) {
            a2=recPrintIndividual(tempProgram,a1);
            std::cout<<")";
            return(a2);
        } else if ((int)tempProgram[bufferCnt-1] > 98 && (int)tempProgram[bufferCnt-1] < 101) {
            a2=recPrintIndividual(tempProgram,a1);
            a3=recPrintIndividual(tempProgram,a2);
            std::cout<<")";
            return(a3);
        }   else if ((int)tempProgram[bufferCnt-1] > 89 && (int)tempProgram[bufferCnt-1] < 92) {
            a2=recPrintIndividual(tempProgram,a1);
            std::cout<<")";
            return(a2);
        } else if ((int)tempProgram[bufferCnt-1] > 117 && (int)tempProgram[bufferCnt-1] < 123) {
            a2=recPrintIndividual(tempProgram,a1);
            a3=recPrintIndividual(tempProgram,a2);
            a4=recPrintIndividual(tempProgram,a3);
            a5=recPrintIndividual(tempProgram,a4);
            std::cout<<")";
            return(a5);
        }  else if ((int)tempProgram[bufferCnt-1] < 90) {
           std::cout<<",";
          return(a1);
        } else {
           std::cout<<")";
          return(a1);
        }
	
}

//method to start the recursive printing of the individual
void Indi::codeIndividual(int mode){
    switch (mode) {
        case 0:
            std::cout << "//+------------------------------------------------------------------+\n";
            std::cout << "//|                                                  Trade_Bench.mq4 |\n";
            std::cout << "//|                            Copyright 2005-2016, Trade Bench Inc. |\n";
            std::cout << "//|                                        http://www.tradebench.com |\n";
            std::cout << "//+------------------------------------------------------------------+\n";
            std::cout << "#property copyright   \"2005-2016, Trade Bench Inc.\"\n";
            std::cout << "#property link        \"http://www.tradebench.com\"\n";
            std::cout << "\n\ninput double Lots = 1.0;\n";
            std::cout << "input int    MagicNumber = 12121212;\n\n";
            std::cout << "int calcRisk = 1;\n";
            std::cout << "double f[100];\n\n";
            
            
            this->recCodeIndividualMT4(1);
            this->outputFooterCodeMT4();
            
            this->PC = 0;

            break;
            
        default:
            break;
    }
}

//the recursive function for traversal and print
int Indi::recCodeIndividualMT4(int branch){
    //std::cout << tempProgram.size() << std::endl;
    
    std::string program = this->program;
    //std::cout << "Run" << std::endl;
    
    char node = program[this->PC];
    
    //std::cout << (int)node << std::endl;
    
    this->PC++;
    
    int decision = 0;
    int then = 0;
    int elseThen = 0;
    int arg1 = 0, arg2 = 0, arg3 = 0, arg4 = 0, arg5 = 0, indicatorIndex = 0;
    int a, b;
    double intpart;
    float fracpart;
    float range = 1.12345;
    float tempResult = 0.0;
    
    //std::cout << PC << std::endl;
    if (node < 90 ){//this->test->FSET_START
        //std::cout << "Returning: " << (int)node << std::endl;
        return ((int)node);
    }
    switch(node){
        case 99:
            //std::cout<<"HEAD ";
            std::cout << "\n//The Evaluation Function" << std::endl;
            std::cout << "void onTick () {" << "\n";
            std::cout << "\tif (newBar()) {\n";
            std::cout << "\t\tArrayInitialize(f, 0);\n";
            recCodeIndividualMT4(1);
            std::cout << "\t\tif (f[1] == 1 ) {" << std::endl <<
            "\t\t\tgoLong();" << std::endl <<
            "\t\t} else {" << std::endl <<
            "\t\t\tgoShort();" << std::endl <<
            "\t\t}" << std::endl;
            std::cout << "\t}" << "\n";
            std::cout << "}" << "\n";
            std::cout << "\n//The Long Trade Function" << std::endl;
            std::cout << "void goLong () {" << "\n";
            std::cout << "\t if(calcRisk) {" << "\n";
            std::cout << "\t\tdouble fracpart = 0;" << "\n";
            //std::cout<<"Should go LONG ";
            recCodeIndividualMT4(1);
            std::cout << "\t} else {" << "\n";
            std::cout << "\t\tdouble LONGSL = 0;" << "\n";
            std::cout << "\t\tdouble LONGTP = 0;" << "\n";
            std::cout << "\t}" << "\n";
            outputLongTradeCodeMT4();
            std::cout << "}" << "\n";
            std::cout << "\n//The Short Trade Function" << std::endl;
            std::cout << "void goShort () {" << "\n";
            std::cout << "\t if(calcRisk) {" << "\n";
            std::cout << "\t\tdouble fracpart = 0;" << "\n";
            //std::cout<<"Should go SHORT ";
            recCodeIndividualMT4(1);
            std::cout << "\t} else {" << "\n";
            std::cout << "\t\tdouble SHORTSL = 0;" << "\n";
            std::cout << "\t\tdouble SHORTTP = 0;" << "\n";
            std::cout << "\t}" << "\n";
            outputShortTradeCodeMT4();
            std::cout << "}" << "\n";
            return 1;
        case 100:
            //std::cout<<"IFELSE ";
            recCodeIndividualMT4(branch);
            recCodeIndividualMT4(branch + 1);
            recCodeIndividualMT4(branch + 2);
            
            std::cout << "\t\tif (f[" << branch << "] == 1 ) {" << "\n" <<
            "\t\t\tf[" << branch << "] = f[" << branch + 1 << "];" << "\n" <<
            "\t\t} else {" <<std::endl <<
            "\t\t\tf[" << branch << "] = f[" << branch + 2 << "];" << "\n" <<
            "\t\t}" << std::endl;
            return 1;
        case 101:
            //std::cout<<"AND ";
            a = recCodeIndividualMT4(branch);
            b = recCodeIndividualMT4(branch + 1);
            std::cout << "\t\tif (f[" << branch << "] == 1 && f[" << branch + 1 << "] == 1) {" << "\n" <<
            "\t\t\t f[" << branch << "] =  1;" << "\n" <<
            "\t\t} else {" << std::endl <<
            "\t\t\t f[" << branch << "] =  -1;" << "\n" <<
            "\t\t}" << std::endl;
            return 1;
        case 102:
            //std::cout<<"OR ";
            a = recCodeIndividualMT4(branch);
            b = recCodeIndividualMT4(branch + 1);
            std::cout << "\t\tif (f[" << branch << "] == 1 || f[" << branch + 1 << "] == 1) {" << "\n" <<
            "\t\t\t f[" << branch << "] =  1;" << "\n" <<
            "\t\t} else {" << std::endl <<
            "\t\t\t f[" << branch << "] =  -1;" << "\n" <<
            "\t\t}" << std::endl;
            return 1;
        case 103:
            //std::cout<<"XOR ";
            a = recCodeIndividualMT4(branch);
            b = recCodeIndividualMT4(branch + 1);
            std::cout << "\t\tif ((f[" << branch << "] == 1 && f[" << branch + 1 << "] == -1)" <<
                "|| (f[" << branch << "] == 1 && f[" << branch + 1 << "] == 1) {" << "\n" <<
            "\t\t\t f[" << branch << "] =  1;" << "\n" <<
            "\t\t} else {" << std::endl <<
            "\t\t\t f[" << branch << "] =  -1;" << "\n" <<
            "\t\t}" << std::endl;
            return 1;
        case 104:
            //std::cout<<"LESS ";
            a = recCodeIndividualMT4(branch);
            b = recCodeIndividualMT4(branch + 1);
            std::cout << "\t\tif (f[" << branch << "] < f[" << branch + 1 << "]) {" << "\n" <<
            "\t\t\t f[" << branch << "] =  1;" << "\n" <<
            "\t\t} else {" << std::endl <<
            "\t\t\t f[" << branch << "] =  -1;" << "\n" <<
            "\t\t}" << std::endl;
            return 1;
        case 105:
            //std::cout<<"GREAT ";
            a = recCodeIndividualMT4(branch);
            b = recCodeIndividualMT4(branch + 1);
            std::cout << "\t\tif (f[" << branch << "] > f[" << branch + 1 << "]) {" << "\n" <<
            "\t\t\t f[" << branch << "] =  1;" << "\n" <<
            "\t\t} else {" << std::endl <<
            "\t\t\t f[" << branch << "] =  -1;" << "\n" <<
            "\t\t}" << std::endl;
            return 1;
        case 106:
            //std::cout<<"EQU ";
            a = recCodeIndividualMT4(branch);
            b = recCodeIndividualMT4(branch + 1);
            std::cout << "\t\tif (f[" << branch << "] == f[" << branch + 1 << "]) {" << "\n" <<
            "\t\t\t f[" << branch << "] =  1;" << "\n" <<
            "\t\t} else {" << std::endl <<
            "\t\t\t f[" << branch << "] =  -1;" << "\n" <<
            "\t\t}" << std::endl;
            return 1;
        case 107:
            //std::cout<<"ADD ";
            a = recCodeIndividualMT4(branch);
            b = recCodeIndividualMT4(branch + 1);
            std::cout << "\t\tf[" << branch << "] += f[" << branch + 1 << "];" << std::endl;
            return 1;
        case 108:
            //std::cout<<"SUB ";
            a = recCodeIndividualMT4(branch);
            b = recCodeIndividualMT4(branch + 1);
            std::cout << "\t\tf[" << branch << "] -= f[" << branch + 1 << "];" << std::endl;
            return 1;
        case 109:
            //std::cout<<"MUL ";
            a = recCodeIndividualMT4(branch);
            b = recCodeIndividualMT4(branch + 1);
            std::cout << "\t\tf[" << branch << "] *= f[" << branch + 1 << "];" << std::endl;
            return 1;
        case 110:
            a = recCodeIndividualMT4(branch);
            b = recCodeIndividualMT4(branch + 1);
            std::cout << "\t\tif (f[" << branch + 1 << "] < 0.000001) {" << "\n" <<
            "\t\t\t f[" << branch << "] =  -1;" << "\n" <<
            "\t\t} else {" << std::endl <<
            "\t\t\t f[" << branch << "] /= f[" << branch + 1 << "];" << "\n" <<
            "\t\t}" << std::endl;
            //std::cout << "\tf[" << branch << "] /= f[" << branch + 1 << "];" << std::endl;
            return 1;
        case 111:
            //std::cout<<"MIN ";
            a = recCodeIndividualMT4(branch);
            b = recCodeIndividualMT4(branch + 1);
            std::cout << "\t\tf[" << branch << "] = MathMin(f[" << branch << "], f[" << branch + 1 <<  "]);" << std::endl;
            return 1;
        case 112:
            //std::cout<<"MAX ";
            a = recCodeIndividualMT4(branch);
            b = recCodeIndividualMT4(branch + 1);
            std::cout << "\t\tf[" << branch << "] = MathMax(f[" << branch << "], f[" << branch + 1 <<  "]);" << std::endl;
            return 1;
        case 113:
            //std::cout<<"NEG ";
            a = recCodeIndividualMT4(branch);
            std::cout << "\t\tf[" << branch << "] = -(f[" << branch << "]);" << std::endl;
            return 1;
        case 114:
            //std::cout<<"SIN ";
            a = recCodeIndividualMT4(branch);
            std::cout << "\t\tf[" << branch << "] = MathSin(f[" << branch << "]);" << std::endl;
            return 1;
        case 115:
            //std::cout<<"COS ";
            a = recCodeIndividualMT4(branch);
            std::cout << "\t\tf[" << branch << "] = MathCos(f[" << branch << "]);" << std::endl;
            return 1;
        case 116:
            //std::cout<<"TAN ";
            a = recCodeIndividualMT4(branch);
            std::cout << "\t\tf[" << branch << "] = MathTan(f[" << branch << "]);" << std::endl;
            return 1;
        case 117:
            //std::cout<<"LOG ";
            a = recCodeIndividualMT4(branch);
            std::cout << "\t\tf[" << branch << "] = MathLog(MathAbs(f[" << branch << "]));" << std::endl;
            return 1;
        case 118:
            //std::cout<<"MA ";
            //indicatorIndex = this->getIndicatorIndex(0);
            //std::cout<<" " << indicatorIndex << " ";
            arg1 = recCodeIndividualMT4(branch);
            arg2 = recCodeIndividualMT4(branch);
            arg3 = recCodeIndividualMT4(branch);
            arg4 = recCodeIndividualMT4(branch);
            arg5 = recCodeIndividualMT4(branch);
            std::cout << "\t\tf[" << branch << "] = MA(" << arg1 << ", " << arg2 << ", " << arg3 << ", " << arg4 + 1 << ", " << arg5 << ");" << std::endl;
            return 1;
        case 119:
            //std::cout<<"MACD ";
            //indicatorIndex = this->getIndicatorIndex(1);
            //std::cout<<" " << indicatorIndex << " ";
            arg1 = recCodeIndividualMT4(branch);
            arg2 = recCodeIndividualMT4(branch);
            arg3 = recCodeIndividualMT4(branch);
            arg4 = recCodeIndividualMT4(branch);
            arg5 = recCodeIndividualMT4(branch);
            std::cout << "\t\tf[" << branch << "] = MACD(" << arg1 << ", " << arg2 << ", " << arg3 << ", " << arg4 + 1 << ", " << arg5 << ");" << std::endl;
            return 1;
        case 120:
            //std::cout<<"STOCH ";
            //indicatorIndex = this->getIndicatorIndex(2);
            //std::cout<<" " << indicatorIndex << " ";
            arg1 = recCodeIndividualMT4(branch);
            arg2 = recCodeIndividualMT4(branch);
            arg3 = recCodeIndividualMT4(branch);
            arg4 = recCodeIndividualMT4(branch);
            arg5 = recCodeIndividualMT4(branch);
            std::cout << "\t\tf[" << branch << "] = STOCH(" << arg1 << ", " << arg2 << ", " << arg3 << ", " << arg4 + 1 << ", " << arg5 << ");" << std::endl;
            return 1;
        case 121:
            //std::cout<<"NEXT ";
            //indicatorIndex = this->getIndicatorIndex(3);
            //std::cout<<" " << indicatorIndex << " ";
            arg1 = recCodeIndividualMT4(branch);
            arg2 = recCodeIndividualMT4(branch);
            arg3 = recCodeIndividualMT4(branch);
            arg4 = recCodeIndividualMT4(branch);
            arg5 = recCodeIndividualMT4(branch);
            std::cout << "\t\tf[" << branch << "] = RSI(" << arg1 << ", " << arg2 << ", " << arg3 << ", " << arg4 + 1 << ", " << arg5 << ");" << std::endl;
            return 1;
        case 122:
            //std::cout<<"LAST ";
            //indicatorIndex = this->getIndicatorIndex(4);
            //std::cout<<" " << indicatorIndex << " ";
            arg1 = recCodeIndividualMT4(branch);
            arg2 = recCodeIndividualMT4(branch);
            arg3 = recCodeIndividualMT4(branch);
            arg4 = recCodeIndividualMT4(branch);
            arg5 = recCodeIndividualMT4(branch);
            std::cout << "\t\tf[" << branch << "] = MOM(" << arg1 << ", " << arg2 << ", " << arg3 << ", " << arg4 + 1 << ", " << arg5 << ");" << std::endl;
            return 1;
        case 123:
            //std::cout<<"OPEN ";
            arg1 = recCodeIndividualMT4(branch);
            std::cout << "\t\tf[" << branch << "] = Open["<< arg1 + 1 <<"];" << std::endl;
            return 1;
        case 124:
            //std::cout<<"HIGH ";
            arg1 = recCodeIndividualMT4(branch);
            std::cout << "\t\tf[" << branch << "] = High["<< arg1 + 1 <<"];" << std::endl;
            return 1;
        case 125:
            //std::cout<<"LOW ";
            arg1 = recCodeIndividualMT4(branch);
            std::cout << "\t\tf[" << branch << "] = Low["<< arg1 + 1 <<"];" << std::endl;
            return 1;
        case 126:
            //std::cout<<"CLOSE ";
            arg1 = recCodeIndividualMT4(branch);
            std::cout << "\t\tf[" << branch << "] = Close["<< arg1 + 1 <<"];" << std::endl;
            return 1;
        case 127:
            //std::cout<<"VOLUME ";
            arg1 = recCodeIndividualMT4(branch);
            std::cout << "\t\tf[" << branch << "] = Volume["<< arg1 + 1 <<"];" << std::endl;
            return 1;
        case 90:
            //std::cout<<"--LONG-- ";
            recCodeIndividualMT4(1);
            std::cout << "\t\tdouble LONGSL = fracpart;" << std::endl;
            //this->workingTrade->setLongSL(fracpart, this->getBarAt(this->currentDataIndex - 1));//this->getBarAt(this->currentDataIndex - 1)
            recCodeIndividualMT4(1);
            std::cout << "\t\tdouble LONGTP = fracpart;" << std::endl;
            //this->workingTrade->setLongTP(fracpart, this->getBarAt(this->currentDataIndex - 1));//this->getCurrentBar()
            return 1;
        case 91:
            //std::cout<<"--SHORT-- ";
            recCodeIndividualMT4(1);
            std::cout << "\t\tdouble SHORTSL = fracpart;" << std::endl;
            //this->workingTrade->setShortSL(fracpart, this->getBarAt(this->currentDataIndex - 1));//this->getCurrentBar()
            recCodeIndividualMT4(1);
            std::cout << "\t\tdouble SHORTTP = fracpart;" << std::endl;
            //this->workingTrade->setShortTP(fracpart, this->getBarAt(this->currentDataIndex - 1));//this->getCurrentBar()
            return 1;
        case 92:
            //std::cout<<"SL ";
            recCodeIndividualMT4(1);
            //range = this->dataStore->getAverageRange() * 2000;
            std::cout << "\t\tfracpart = fracPart(f["<< branch <<"]);" <<std::endl;
            std::cout << "\t\tfracpart = MathAbs(fracpart) * " << range << ";" << std::endl;
            return 1;
        case 93:
            //std::cout<<"TP ";
            recCodeIndividualMT4(1);
            //range = this->dataStore->getAverageRange() * 2000;
            std::cout << "\t\tfracpart = fracPart(f["<< branch <<"]);" <<std::endl;
            std::cout << "\t\tfracpart = MathAbs(fracpart) * " << range << ";" << std::endl;
            return 1;
    }
    return (0);
}

void Indi::outputFooterCodeMT4() {
    
    std::cout << "\nbool newBar() {\n";
    std::cout << "\tstatic int prevTime = 0;\n";
    std::cout << "\tif (prevTime == iTime(Symbol(),0,0)) {\n";
    std::cout << "\t\treturn false;\n";
    std::cout << "\t} else {\n";
    std::cout << "\t\treturn true;\n";
    std::cout << "\t}\n";
    std::cout << "}\n";
    
    std::cout << "\ndouble fracPart(double value) {\n";
    std::cout << "\tdouble result = value - ((long)value);\n";
    std::cout << "\treturn result;\n";
    std::cout << "}\n";
    
    std::cout << "\nint checkShort() {\n";
    std::cout << "\tint cnt;\n";
    std::cout << "\ttotal = OrdersTotal();\n";
    std::cout << "\tfor(cnt = 0; cnt < total; cnt++) {\n";
    std::cout << "\t\tOrderSelect(cnt, SELECT_BY_POS, MODE_TRADES);\n";
    std::cout << "\t\tif(OrderType() == OP_SELL && OrderSymbol() == Symbol() && OrderMagicNumber() == MagicNumber) {\n";
    std::cout << "\t\t\treturn OrderTicket();\n";
    std::cout << "\t\t}\n";
    std::cout << "\t}\n";
    std::cout << "\treturn -1;\n";
    std::cout << "}\n";
    
    std::cout << "\nint checkLong() {\n";
    std::cout << "\tint cnt;\n";
    std::cout << "\ttotal = OrdersTotal();\n";
    std::cout << "\tfor(cnt = 0; cnt < total; cnt++) {\n";
    std::cout << "\t\tOrderSelect(cnt, SELECT_BY_POS, MODE_TRADES);\n";
    std::cout << "\t\tif(OrderType() == OP_BUY && OrderSymbol() == Symbol() && OrderMagicNumber() == MagicNumber) {\n";
    std::cout << "\t\t\treturn OrderTicket();\n";
    std::cout << "\t\t}\n";
    std::cout << "\t}\n";
    std::cout << "\treturn -1;\n";
    std::cout << "}\n";

}

void Indi::outputLongTradeCodeMT4() {
    std::cout << "\tint order = checkLong();\n";
    std::cout << "\tif (order > 0) {\n";
    std::cout << "\t\tif(calcRisk) {\n";
    std::cout << "\t\t\tOrderModify(order, OrderOpenPrice(), Bid-Point*LONGSL, Bid+Point*LONGTP, 0, Green);\n";
    std::cout << "\t\t\treturn;\n";
    std::cout << "\t\t} else {\n";
    std::cout << "\t\t\treturn;\n";
    std::cout << "\t\t}\n";
    std::cout << "\t}\n";
    std::cout << "\torder = checkShort();\n";
    std::cout << "\twhile (order > 0) {\n";
    std::cout << "\t\tOrderClose(order, OrderLots(), Bid, 2, Violet);\n";
    std::cout << "\t\torder = checkShort();\n";
    std::cout << "\t}\n";
    std::cout << "\tint ticket = OrderSend(Symbol(), OP_BUY, Lots, Ask, 2, ask-LONGSL*Point, Ask+LONGTP*Point, \"trade bench\", MagicNumber, 0, Green);\n";
    std::cout << "\tif(ticket>0) {\n";
    std::cout << "\t\tif(OrderSelect(ticket,SELECT_BY_TICKET,MODE_TRADES)) Print(\"BUY order opened : \", OrderOpenPrice());\n";
    std::cout << "\t} else {\n";
    std::cout << "\t\tPrint(\"Error opening BUY order : \", GetLastError());\n";
    std::cout << "\t}\n";
}

void Indi::outputShortTradeCodeMT4() {
    std::cout << "\tint order = checkShort();\n";
    std::cout << "\tif (order > 0) {\n";
    std::cout << "\t\tif(calcRisk) {\n";
    std::cout << "\t\t\tOrderModify(order, OrderOpenPrice(), Ask+Point*SHORTSL, Ask-Point*SHORTTP, 0, Red);\n";
    std::cout << "\t\t\treturn;\n";
    std::cout << "\t\t} else {\n";
    std::cout << "\t\t\treturn;\n";
    std::cout << "\t\t}\n";
    std::cout << "\t}\n";
    std::cout << "\torder = checkLong();\n";
    std::cout << "\twhile (order > 0) {\n";
    std::cout << "\t\tOrderClose(order, OrderLots(), Ask, 3, Violet);\n";
    std::cout << "\t\torder = checkLong();\n";
    std::cout << "\t}\n";
    std::cout << "\tint ticket = OrderSend(Symbol(), OP_SELL, Lots, Bid, 2, Bid+SHORTSL*Point, Bid-SHORTTP*Point, \"trade bench\", MagicNumber, 0, Red);\n";
    std::cout << "\tif(ticket>0) {\n";
    std::cout << "\t\tif(OrderSelect(ticket,SELECT_BY_TICKET,MODE_TRADES)) Print(\"SELL order opened : \", OrderOpenPrice());\n";
    std::cout << "\t} else {\n";
    std::cout << "\t\tPrint(\"Error opening SELL order : \", GetLastError());\n";
    std::cout << "\t}\n";
}










