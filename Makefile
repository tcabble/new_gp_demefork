CC = g++

CFLAGS = -std=c++11 -pthread -g

LIB = ${PWD}/LIB-TA

LIBHOME = ${PWD}/LIB-TA_Centos

LDHOMEFLAGS += -L$(LIBHOME)

LDHOMEFLAGS += -Wl,-Bstatic -lta_lib -Wl,-Bdynamic

LDFLAGS += -L$(LIB)

LDFLAGS += -lta_lib

GPCLASSES =                 \
	tester.cpp				\
	ChartData.cpp           \
	Evaluator.cpp           \
	Engine.cpp				\
	GPTest.cpp 				\
	Indi.cpp 				\
	Run.cpp  				\
	FitnessFunction.cpp		\
	Trade.cpp				\
	Ledger.cpp				\
	TradeManager.cpp

TACLASSES =                 \
	codeTester.cpp			\
	ChartData.cpp           \
	Evaluator.cpp           \
	Engine.cpp				\
	GPTest.cpp 				\
	Indi.cpp 				\
	Run.cpp  				\
	FitnessFunction.cpp		\
	Trade.cpp				\
	Ledger.cpp				\
	TradeManager.cpp

OBJECTS = $(GPCLASSES:.cpp=.o)

TAOBJECTS = $(TACLASSES:.cpp=.o)

test: $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) $(LDFLAGS) -o $@
	
test_home: $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) $(LDHOMEFLAGS) -o $@

ta_test: $(TAOBJECTS)
	$(CC) $(CFLAGS) $(TAOBJECTS) $(LDFLAGS) -o $@

code_test: $(TAOBJECTS)
	$(CC) $(CFLAGS) $(TAOBJECTS) $(LDFLAGS) -o $@

code_test_home: $(TAOBJECTS)
	$(CC) $(CFLAGS) $(TAOBJECTS) $(LDHOMEFLAGS) -o $@

.cpp.o:
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -f *.o test ta_test ta_test_home code_test
	
