//
//  ta-lib_test.cpp
//  
//
//  Created by Tim Cabble on 1/15/16.
//
//

#include <time.h>
#include <thread>
#include <mutex>
#include <random>
#include <chrono>
#include "Run.hpp"
#include "GPTest.hpp"
#include "Engine.hpp"
#include "Evaluator.hpp"
#include "Trade.hpp"
#include "TradeManager.hpp"




void launch(ChartData dataStore, Engine *theEngine, int seed, int index);

void testThread(int index);

int main(int argc, char *argv[]){
    ChartData dataStore;
    char *filename = argv[1];
    dataStore.loadData(filename);
    
    Evaluator evaluator(&dataStore, 3);

    std::function<float(int&, int&, int&, int&, int&)> indicator;

    indicator = evaluator.getIndicator(6);

   	int arg1 = 14;
    int arg2 = 12;
    int arg3 = 9;
    int arg4 = 0;
    int arg5 = 0;

    float test = indicator(arg1, arg2, arg3, arg4, arg5);
    
    //float test = evaluator.ta_libTest(24, 0);
    
    std::cout << "The value is " << test << std::endl;
}