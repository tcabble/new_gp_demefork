#include "Engine.hpp"

//std::mutex  Engine::mtx;

bool final_sort_on_test(const Indi &indi1, const Indi &indi2) {
    return indi1.testReturn > indi2.testReturn;
}

Engine::Engine(int numDemes) {
    this->numDemes = numDemes;
    for (int i = 0; i < this->numDemes; ++i) {
        std::queue<Indi> newQueue;
        this->travelers.push_back(newQueue);
    }    
}

Engine::~Engine() {

}

Deme Engine::getDemeStruct(int index) {
    //std::cout << "Size is: " << this->theDemes.size() << std::endl;
	return this->theDemes.at(index);
}

void Engine::demeIsDone(int index) {
    this->theDemes.at(index).is_done = true;
}

void Engine::demeIsReady(int index) {
    this->theDemes.at(index).ready_to_run = true;
}

bool Engine::allDemesReady() {
    for (Deme it : this->theDemes) {
        if (it.ready_to_run == false) {
            return false;
        }
    }
    return true;
}

std::vector<Deme> Engine::getDemeStructs() {
    return this->theDemes;
}

void Engine::addDemeStruct(Deme theDeme) {
    this->theDemes.push_back(theDeme);
}

std::vector<std::queue<Indi> > Engine::getTravelers() {
    return this->travelers;
}

void Engine::enqueueIndividual(int index, Indi indiv) {
    
    //std::lock_guard<std::mutex> lock(mtx);
    //m.lock();
    this->travelers.at(index).push(indiv);
    //std::cout << "Adding Indiv to Queue: " << index << " size: " << this->travelers.at(index).size() << std::endl;
    //m.unlock();
}
	
std::vector<Indi> Engine::migrateIndividuals(int index) {
    
    //std::lock_guard<std::mutex> lock(mtx);
    //m.lock();
    //std::cout << "Indiv coming into Queue: " << index << " Size: " << this->travelers.at(index).size() << std::endl;
    std::vector<Indi> tempTravelers;
	while (!this->travelers.at(index).empty()) {
		Indi traveler = this->travelers.at(index).front();
		tempTravelers.push_back(traveler);
		this->travelers.at(index).pop();
	}
    //m.unlock();
    return tempTravelers;
}

void Engine::addDemeTest(GPTest *deme) {
	this->demeRuns.push_back(deme);
}

void Engine::addIndividual(Indi indiv) {
    this->bestIndividuals.push_back(indiv);
}

Indi Engine::getIndividual(int index) {
    return this->bestIndividuals.at(index);
}

void Engine::sortIndividuals() {
    std::sort(this->bestIndividuals.begin(), this->bestIndividuals.end(), final_sort_on_test);
}

void Engine::printResults() {
    for (Indi it : this->bestIndividuals) {
        std::cout << "This Individual test return: " << it.testReturn << std::endl;
    }
}

void Engine::printCode(int index) {
    this->bestIndividuals.at(index).codeIndividual(0);
}

void Engine::setGensWithout(int index, int value) {
	//this->getDemeStruct(index).gens_without_imp = value;
    this->theDemes.at(index).gens_without_imp = value;
}

void Engine::setPopulationSize(int index, int value) {
	//this->getDemeStruct(index).populationSize = value;
    this->theDemes.at(index).populationSize = value;
}

void Engine::setMaxLength(int index, int value) {
	this->theDemes.at(index).maxLength = value;
}

void Engine::setMinLength(int index, int value) {
	this->theDemes.at(index).minLength = value;
}

void Engine::setMaxDepth(int index, int value) {
	this->theDemes.at(index).maxDepth = value;
}

void Engine::setCrossoverProb(int index, int value) {
	this->theDemes.at(index).crossover_prob = value;
}

void Engine::setTournSize(int index, int value) {
	this->theDemes.at(index).tourn_size = value;
}

void Engine::setTrainingRange(int index, std::pair <int, int> values) {
	this->theDemes.at(index).trainingRange = values;
}

void Engine::setValidationRange(int index, std::pair <int, int> values) {
	this->theDemes.at(index).validationRange = values;
}

void Engine::setTestingRange(int index, std::pair <int, int> values) {
	this->theDemes.at(index).testingRange = values;
}