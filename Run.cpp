/*
 * Run.cpp
 *
 *  Created on: Feb 11, 2014
 *      Author: Tim
 *
 * Implementation of the Run class
 */

#include "Run.hpp"
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>
#include <algorithm>

std::mutex  Run::mtx;

bool sort_on_test(const Indi &indi1, const Indi &indi2) {
    return indi1.testReturn > indi2.testReturn;
    //return indi1.valEquity > indi2.valEquity;
}

//constructor
Run::Run(GPTest *theTest, int runNo) {

	test = theTest;
	bestFitness = -99999999.0;
	bestLength = 0;
	this->runNo = runNo;
}

Run::~Run() {
	// TODO Auto-generated destructor stub
}

int Run::getRunNumber() {
	return this->runNo;
}

void Run::setRunNumber(int runNo) {
	this->runNo = runNo;
}

std::thread Run::launchRun() {
	return std::thread(&Run::evolve, this);
}


//Helper to grow individuals for the population
void Run::createRandomIdividual(Indi &indiv){
	
    int len = 0;
	std::string ind;
	//std::cout << "Random Indiv" << std::endl;
	len=grow(&ind,0,this->test->maxDepth, "root");

    
	while (len > this->test->maxLength)
	{	   
	   //std::cout << "Growing algo" << std::endl;
	   ind.clear();
	   len=grow(&ind, 0, this->test->maxDepth, "root");
	}
	//std::cout << "Done Growing algo" << std::endl;
	growTrade(&ind, ind.size(), this->test->maxDepth, "long");
	growTrade(&ind, ind.size(), this->test->maxDepth, "short");
	//std::cout << "Done Growing trade" << std::endl;
    for (int i = 0; i < 3; ++i) {
        Ledger newLedger;
        indiv.setLedger(newLedger, i);
    }
    indiv.program = ind;
    
    
}

void Run::testThread() {
	while(1) {

	}
}


//instantiates Indi objs and puts them in the population vector
void Run::createRandomPopulation(){
	std::cout << "creating population" << std::endl;
	
	for (int i = 0; i < test->populationSize; i++)
	{
		float dist = 0;
		while (dist < 0.05) {
			Indi newIndiv;
			std::stringstream stream;
			stream << "------------------- Trying New Individual: "<< i+1 << " Deme: " << this->getRunNumber() << " -----------------";
	        std::cout << stream.str() << std::endl;
	        //std::cout << this->getRunNumber() << std::endl;
			createRandomIdividual(newIndiv);
			//std::cout << "Created" << std::endl;
			this->test->function->calcFitness(this->test->getEvaluator(), newIndiv, this->test->getDeme());
			this->test->function->calcValFitness(this->test->getEvaluator(), newIndiv, this->test->getDeme());
			//this->test->function->calcFitness(this->test->valInputs, newIndiv, 1);
			this->test->progsEvaled++;
			dist = newIndiv.getLedgers(0)->getTradeDistribution();
            //std::cout << "Distribution = " << dist << std::endl;
			if (dist > 0.05) {
                //std::cout << "Pushing" << std::endl;
                //std::cout << "Size: " << this->population.size() << std::endl;
				this->population.push_back(newIndiv);
                //std::cout << "Pushed" << std::endl;
				break;
			}
		}
	}
    this->test->getEngine()->demeIsReady(this->getRunNumber());
}


//the recursive function for randomly creating an individual program trade rule
int Run::grow(std::string *buffer,int pos,int depth, std::string type){
    //std::cout << "Grow" << std::endl;
    int random = 0;
    char node = (char)test->nextInt(100);
    
    if (depth < 0) {
        return 1000;
    }

	if (type == "root") {
		node = (char)99;
        //std::cout << (int)node << std::endl;
		buffer->push_back(node);
		return  grow(buffer, pos+1, depth-1, "bool");
	} else if (type == "bool") {
		node= (char) test->nextInt(test->EQU - test->AND + 1) + test->AND;
		switch (node){
            
			case 101:
			case 102:
			case 103:
				buffer->push_back(node);
				return(grow(buffer, grow(buffer, pos+1, depth-1, "bool"), depth-1, "bool"));
			case 104:
			case 105:
			case 106:
				buffer->push_back(node);
				return(grow(buffer, grow(buffer, pos+1, depth-1, "any"), depth-1, "any"));
		}
		
	} else if (node < 50 || depth <= 0) {
	  	node = (char) test->nextInt(test->VOLUME - test->MA + 1) + test->MA;
		switch (node) {
            case 118:
                //buffer->push_back(node);
                //buffer->push_back((char)test->nextInt(2));
                //buffer->push_back((char)(test->nextInt(40 - 3) + 3));
                //buffer->push_back((char)test->nextInt(40));
                //return (pos + 4);
            case 119:
                //buffer->push_back(node);
                //buffer->push_back((char)(test->nextInt(32 - 3) + 3));
                //buffer->push_back((char)(test->nextInt(15 - 3) + 3));
                //buffer->push_back((char)(test->nextInt(15 - 3) + 3));
                //buffer->push_back((char)test->nextInt(40));
                //buffer->push_back((char)test->nextInt(2));
                //return (pos + 6);
            case 120:
                //buffer->push_back(node);
                //buffer->push_back((char)(test->nextInt(20 - 3) + 3));
                //buffer->push_back((char)(test->nextInt(12 - 3) + 3));
                //buffer->push_back((char)(test->nextInt(12 - 3) + 3));
                //buffer->push_back((char)test->nextInt(40));
                //buffer->push_back((char)test->nextInt(2));
                //return (pos + 6);
			case 121:
			case 122:
				//buffer->push_back(node);
				//buffer->push_back((char)(test->nextInt(40 - 3) + 3));
				//buffer->push_back((char)test->nextInt(40));
				//return (pos + 3);
                buffer->push_back(node);
                buffer->push_back((char)(test->nextInt(32 - 3) + 3));
                buffer->push_back((char)(test->nextInt(15 - 3) + 3));
                buffer->push_back((char)(test->nextInt(15 - 3) + 3));
                buffer->push_back((char)test->nextInt(40));
                buffer->push_back((char)test->nextInt(4));
                return (pos + 6);
			case 123:
			case 124:
			case 125:
			case 126:
			case 127:
			  	buffer->push_back(node);
				buffer->push_back((char)test->nextInt(40));
				return (pos + 2);
		}

	} else {
		node = 0;
		while (true){
			node=(char) test->nextInt(test->LOG - test->IFELSE + 1)+test->IFELSE;
			if (node == 100 || (node > 106 && node < 118)) break;
		}
		
        //std::cout << "Func" << (int)node << std::endl;
		switch(node) {
			case 100:
                buffer->push_back(node);
                return(grow(buffer, grow(buffer,  grow(buffer, pos+1, depth-1, "bool"), depth-1, "any"), depth-1, "any"));
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
                buffer->push_back(node);
                return(grow(buffer, grow(buffer, pos+1, depth-1, "any"), depth-1, "any"));

            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
                buffer->push_back(node);
                return(grow(buffer, pos+1, depth-1, "any"));
		}
	}

	return (0);	
}


//the recursive function for randomly creating an individual program trade parameters
int Run::growTrade(std::string *buffer,int pos,int depth, std::string type){

	char node = (char)test->nextInt(100);


    if (type == "long") {
        node = (char)90;
        buffer->push_back(node);
        return(growTrade(buffer, growTrade(buffer, pos + 1, depth - 1, "SL"), depth - 1, "TP"));
    } else if (type == "short") {
        node = (char)91;
        buffer->push_back(node);
        return(growTrade(buffer, growTrade(buffer, pos + 1, depth - 1, "SL"), depth - 1, "TP"));
    } else if (type == "SL") {
        node = (char)92;
        buffer->push_back(node);
        return(growTrade(buffer, pos + 1, depth - 1, "any"));
    } else if (type == "TP") {
        node = (char)93;
        buffer->push_back(node);
        return(growTrade(buffer, pos + 1, depth - 1, "any"));
    } else if (node < 50 || depth <= 0) {
	  	node= (char) test->nextInt(test->VOLUME - test->MA + 1) + test->MA;
		switch (node) {
            case 118:
                //buffer->push_back(node);
                //buffer->push_back((char)test->nextInt(2));
                //buffer->push_back((char)(test->nextInt(40 - 3) + 3));
                //buffer->push_back((char)test->nextInt(40));
                //return (pos + 4);
            case 119:
                //buffer->push_back(node);
                //buffer->push_back((char)(test->nextInt(32 - 3) + 3));
                //buffer->push_back((char)(test->nextInt(15 - 3) + 3));
                //buffer->push_back((char)(test->nextInt(15 - 3) + 3));
                //buffer->push_back((char)test->nextInt(40));
                //buffer->push_back((char)test->nextInt(2));
                //return (pos + 6);
            case 120:
                //buffer->push_back(node);
                //buffer->push_back((char)(test->nextInt(20 - 3) + 3));
                //buffer->push_back((char)(test->nextInt(12 - 3) + 3));
                //buffer->push_back((char)(test->nextInt(12 - 3) + 3));
                //buffer->push_back((char)test->nextInt(40));
                //buffer->push_back((char)test->nextInt(2));
                //return (pos + 6);
            case 121:
            case 122:
                //buffer->push_back(node);
                //buffer->push_back((char)(test->nextInt(40 - 3) + 3));
                //buffer->push_back((char)test->nextInt(40));
                //return (pos + 3);
                buffer->push_back(node);
                buffer->push_back((char)(test->nextInt(32 - 3) + 3));
                buffer->push_back((char)(test->nextInt(15 - 3) + 3));
                buffer->push_back((char)(test->nextInt(15 - 3) + 3));
                buffer->push_back((char)test->nextInt(40));
                buffer->push_back((char)test->nextInt(4));
                return (pos + 6);
			case 123:
			case 124:
			case 125:
			case 126:
			case 127:
			  	buffer->push_back(node);
				buffer->push_back((char)test->nextInt(40));
				return (pos + 2);
		}

	} else {
		node=(char) test->nextInt(test->FSET_END-test->FSET_START+1)+test->FSET_START;
        //std::cout << "Func " << (int)node << std::endl;
        switch(node) {
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
                buffer->push_back(node);
                return(growTrade(buffer, growTrade(buffer, pos+1, depth-1, "any"), depth-1, "any"));
                
            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
                buffer->push_back(node);
                return(growTrade(buffer, pos+1, depth-1, "any"));
        }
	}
	return (0);	
}


//Inorder traversal of the flattened program tree
int Run::traverse(std::string buffer, int bufferCount){
	//std::cout << "Buffer Count: " << bufferCount << std::endl;
	if (buffer[bufferCount] < 90) {
		return ++bufferCount;
	}

	if (buffer[bufferCount] > 117 && buffer[bufferCount] < 128) {
		switch ((int)buffer[bufferCount]) {
			case 118:
				//++bufferCount;
	      		//return (traverse(buffer, traverse(buffer, traverse(buffer, bufferCount))));
			case 119:
			case 120:
				//++bufferCount;
	      		//return (traverse(buffer, traverse(buffer, traverse(buffer, traverse(buffer, traverse(buffer, bufferCount))))));
			case 121:
			case 122:
                ++bufferCount;
                return (traverse(buffer, traverse(buffer, traverse(buffer, traverse(buffer, traverse(buffer, bufferCount))))));
				//++bufferCount;
				//return traverse(buffer, traverse(buffer, bufferCount));
			case 123:
			case 124:
			case 125:
			case 126:
			case 127:
				//return (bufferCount + 2);
			 	++bufferCount;
               return(traverse(buffer,bufferCount));
		}
	} else {
	   switch((int)buffer[bufferCount])
	   {
	      	case 99:
	      	case 100:
	      		++bufferCount;
	      		return (traverse(buffer, traverse(buffer, traverse(buffer, bufferCount))));
	      	case 101:
	      	case 102:
	      	case 103:
	      	case 104:
		  	case 105:
		  	case 106:
		  	case 107:
		  	case 108:
		  	case 109:
		  	case 110:
		  	case 111:
            case 112:
	    	  	++bufferCount;
	            return(traverse(buffer,traverse(buffer,bufferCount)));
	      	case 113:
	      	case 114:
	      	case 115:
	      	case 116:
            case 117:
               ++bufferCount;
               return(traverse(buffer,bufferCount));
            case 90:
            case 91:
               ++bufferCount;
               return(traverse(buffer,traverse(buffer,bufferCount)));
	      	case 92:
		  	case 93:
	    	  	++bufferCount;
	          	return(traverse(buffer,bufferCount));
	   	}
	}
	return(0);
}


//function to select a parent for crossover or mutation
int Run::tournament(){
	//std::cout << "Tourn" << std::endl;
	int i, competitor, best = this->test->nextInt(this->population.size());
    //std::cout << "first Random " << this->population.size() << std::endl;
	double fbest = -11111111.0;

	for (i = 0; i < this->test->tourn_size; i++) {
	  competitor = this->test->nextInt(this->population.size());
        //std::cout << "second Random " << this->population.size() << std::endl;
	  if(this->population.at(competitor).fitness > fbest) {
	     fbest = this->population.at(competitor).fitness;
	     best = competitor;
	  }
	}
	return(best);
}


//function to select an individual to kill off and replace
int Run::negative_tournament(){
	//std::cout << "Neg Tourn" << std::endl;
	int i, competitor, worst = this->test->nextInt(this->population.size());
	double fworst = 111111111.0;

	for (i = 0; i < this->test->tourn_size; i++){ 
		competitor = this->test->nextInt(this->population.size());
		if(this->population.at(competitor).fitness < fworst) {
		 fworst = this->population.at(competitor).fitness;
		 worst = competitor;
		}
	}
	return(worst);
}


//function to crossover two parents
void Run::crossover(int parent1, int parent2, int newindiv){
	//std::cout << "Cross" << std::endl;
    int xo1start,xo1end,xo2start,xo2end;
	std::string tempchild = "";

	std::vector<int> position;

	std::string indiv1 = this->population.at(parent1).program;

	std::string indiv2 = this->population.at(parent2).program;
	//get all the pieces

	//std::cout << "Pre Traverse" << std::endl;
	int len1 = traverse(indiv1,0);
	int len2 = traverse(indiv2,0);
	int lenoff = 0;

	xo1start = this->test->nextInt(len1-1)+1;

	//std::cout << "Start: " << xo1start << std::endl;

	if ((int)indiv1.at(xo1start) < 90 ) { // returns terminal
		//std::cout << "Terminal" << std::endl;
		//for (char it : indiv2) {
		//	if ((int)it < 90) {
		//		position.push_back((int)it);
		//	}
		//}
		//int location = this->test->nextInt(position.size());
		//xo2start = position.at(location);
		
		
		xo2start = this->test->nextInt(len2);
		//std::cout << (int)indiv2.at(xo2start) << std::endl;
        while ((int)indiv2.at(xo2start) > 89) {
			xo2start = this->test->nextInt(len2);
			//std::cout << (int)indiv2.at(xo2start) << std::endl;
        }
        //std::cout << "start 2: " << xo2start << std::endl;
    }

    if ((int)indiv1.at(xo1start) > 100 && (int)indiv1.at(xo1start) < 107 ) { //Returns Bool
    	//std::cout << "Bool" << std::endl;
		int node = this->test->nextInt(106 - 100 + 1) + 101;
		//std::cout << node << std::endl;
		xo2start = indiv2.find((char)node);
        while (xo2start == -1) {
        	node = this->test->nextInt(106 - 100 + 1) + 101;
			xo2start = indiv2.find((char)node);			
        }
        //std::cout << "start 2: " << xo2start << std::endl;
    }

    if ((int)indiv1.at(xo1start) == 100) {
    	//std::cout << "IF Else" << std::endl;
    	int node = this->test->nextInt(127 - 107 + 1) + 107;
    	//std::cout << node << std::endl;
    	xo2start = indiv2.find((char)node);
        while (xo2start == -1) {
        	node = this->test->nextInt(127 - 107 + 1) + 107;
        	//std::cout << node << std::endl;
			xo2start = indiv2.find((char)node);
        }
        //std::cout << "start 2: " << xo2start << std::endl;
    }

    if ((int)indiv1.at(xo1start) > 106 && (int)indiv1.at(xo1start) < 128) {
    	//std::cout << "Others" << std::endl;
    	int node = this->test->nextInt(127 - 107 + 1) + 107;
    	//std::cout << node << std::endl;
    	xo2start = indiv2.find((char)node);
         while (xo2start == -1) {
        	node = this->test->nextInt(127 - 107 + 1) + 107;
        	//std::cout << node << std::endl;
			xo2start = indiv2.find((char)node);
        }
        //std::cout << "start 2: " << xo2start << std::endl;
    }

    if ((int)indiv1.at(xo1start) > 89 && (int)indiv1.at(xo1start) < 94) {
    	//std::cout << "Trades" << std::endl;
    	
    	//std::cout << (int)indiv1.at(xo1start) << std::endl;
    	xo2start = indiv2.find(indiv1.at(xo1start));

    	if (xo2start == -1) {
    		exit (EXIT_FAILURE);
    	}
        
        //std::cout << "start 2: " << xo2start << std::endl;
    }
    //std::cout << "going to traverse 1" << std::endl;
	xo1end = traverse(indiv1,xo1start);
	//std::cout << "end 1: " << xo1end << std::endl;

	//xo2start = this->test->nextInt(len2);
	//std::cout << "Going to traverse 2" << std::endl;
	xo2end = traverse(indiv2,xo2start);
	//std::cout << "end 2: " << xo2end << std::endl;

	lenoff = xo1end-xo1start+xo2end-xo2start;
	//std::cout << "Make the child" << std::endl;
	//create the new ind from the strings above
	tempchild.insert(tempchild.begin(),indiv1.begin(),indiv1.begin()+xo1start);
	tempchild.insert(tempchild.begin()+xo1start,indiv2.begin()+xo2start,indiv2.begin()+xo2start+(xo2end-xo2start));
	tempchild.insert(tempchild.begin()+xo1start+(xo2end-xo2start),indiv1.begin()+xo1start+(xo1end-xo1start),indiv1.end());

	//lenoff = traverse(tempchild,0);
    
	//not to small or to big
	//if(lenoff < this->test->minLength || lenoff > this->test->maxLength)
	//	return;
    
	//set everything
	this->population.at(newindiv).program = tempchild;

	this->population.at(newindiv).valFitness = 0.0;

	//this->population.at(newindiv).printIndividual();
    //std::cout << "Evaluating" << std::endl;
	this->test->function->calcFitness(this->test->getEvaluator(), this->population.at(newindiv), this->test->getDeme());
	this->test->function->calcValFitness(this->test->getEvaluator(), this->population.at(newindiv), this->test->getDeme());
	this->test->progsEvaled++;

	
	//std::cout << std::endl;

	//this->population.at(newindiv).fitness = this->population.at(newindiv).fitness - fabs(this->population.at(newindiv).fitness - this->population.at(newindiv).valFitness);
}


//function to mutate an indiv
void Run::mutate(int parent, int newIndiv){
	
	int mutsite, i, len;

	std::string parentcopy;
	//get the random spot
	int newRand = this->test->nextInt(this->test->FSET_START );

	parentcopy = this->population.at(parent).program;

	len=parentcopy.size();
/*
	//find it and replace it
	for(i = 0;i < len; i++){
	  if(this->test->nextDbl() < this->test->permutation) {
		 mutsite=i;

		 if (parentcopy.at(mutsite) < this->test->FSET_START)
			parentcopy.at(mutsite) = (char) this->test->nextInt(this->test->varNumber);
		 else
			switch(parentcopy.at(mutsite)){
			   case 110:
			   case 111:
			   case 112:
			   case 113:
			   case 114:
			   case 115:
			   case 116:
			   case 117:
			   case 118:
					parentcopy.at(mutsite)=(char) (this->test->nextInt(this->test->FSET_END-this->test->FSET_START+1)+this->test->FSET_START);
					break;
			   case 119:
			   case 120:
			   case 121:
				   parentcopy.at(mutsite)=(char) (this->test->nextInt(this->test->FSET_END-this->test->COS+1)+this->test->COS);
			}
	  }

	  //set the new indiv
	  this->population.at(newIndiv)->program = parentcopy;
	  //this->test->function->calcFitness(this->test->inputs, this->population.at(newIndiv), 0);
	  //this->test->function->calcFitness(this->test->valInputs, this->population.at(newIndiv), 1);

	  this->population.at(newIndiv)->fitness = this->population.at(newIndiv)->fitness - fabs(this->population.at(newIndiv)->fitness - this->population.at(newIndiv)->valFitness);	  
	}
*/
}



//gather stats from the population		
void Run::stats(int gen){
    
    if(this->population.size() == 0) {
        this->test->gens++;
        return;
    }
	
    int best=this->population.size()/2;
	//int node_count = 0;
	//int ave_len;
	//bool flag = false;
	float fbestpop = this->population.at(best).fitness;
	//float favpop=0.0;

	for (int i = 0; i < this->population.size(); ++i) {
		//node_count += this->population.at(i)->program.length();
		//favpop += this->population.at(best)->fitness;
		if(this->population.at(i).fitness > fbestpop)
		{
            fbestpop = this->population.at(i).fitness;
            best = i;
		}
	}
    std::cout << "Best Fitness= " << this->population.at(best).fitness << std::endl;
    std::cout << "Programs Evaluated= " << this->test->progsEvaled << std::endl;
    this->test->bestIndividualIndex = best;

/*
	ave_len = node_count/this->test->populationSize;

	favpop/=this->test->populationSize;
*/
	if (this->test->currentBestFitness < fbestpop){
        this->test->currentBestFitness = fbestpop;
        std::cout << "--------------------------------Improvement  " << fbestpop <<"   Gen#  "<< this->test->gens << " Deme:" << this->getRunNumber() << " ------------------------" << std::endl;
        this->test->bestIndividualIndex = best;
        this->test->gens = 0;
    } else {
        this->test->gens++;
    }
}



//function to produce the evolution of the indis
void Run::evolve() {
	std::cout << "evolving class: "  << std::endl;
	std::string p1;
	std::string p2;
	int gen=0,offspring,parent1,parent2,run=0;
	float newfit;
    
    stats(this->test->gens);

   while(this->test->gens < this->test->gen_wout_Imp){
      for(int indivs = 0; indivs < this->population.size(); indivs++){
      		float next = this->test->nextDbl();//this->test->populationSize
      		//std::cout << next << std::endl;
    	  if(next < this->test->crossover_prob){
        	parent1 = this->tournament();
			parent2 = this->tournament();
			p1 = this->population.at(parent1).program;
			p2 = this->population.at(parent2).program;
			while (p1 == p2){				
				parent2 = this->tournament();
				p2 = this->population.at(parent2).program;
			}
			//this->population.at(parent1).printIndividual();
			//std::cout << std::endl;
			//this->population.at(parent2).printIndividual();
			//std::cout << std::endl;
            offspring = negative_tournament();
            crossover(parent1, parent2, offspring);
         } 
        //else if (this->test->nextDbl() < this->test->permutation){
        //	parent1 = tournament();
        //    offspring = negative_tournament();
        //    mutate(parent1, offspring);
        // }
      }
       std::cout << "generation= " << this->test->gens << " deme: " << this->getRunNumber() << std::endl;
       //stats(this->test->gens);
       Run::mtx.lock();
       this->migrate();
       std::vector<Indi> naturalizedTravelers;
       std::vector<Indi> tempTravelers = this->test->getEngine()->migrateIndividuals(this->getRunNumber());
/*
       for (Indi it : tempTravelers) {
           //std::pair<int, int> range = std::make_pair(this->test->getDeme()->trainingRange.first, this->test->getDeme()->trainingRange.second);
           //std::cout << "Checking Travelers------- " << this->getRunNumber() << std::endl;
           //it.getLedgers(0)->outputTrades();
           //std::cout << "Fitness in Run: " << it.fitness << std::endl;
           //std::cout << "Ranges in Run: " << range.first << ":" << range.second << std::endl;
           this->test->function->calcFitness(this->test->getEvaluator(), it, this->test->getDeme());
           this->test->function->calcValFitness(this->test->getEvaluator(), it, this->test->getDeme());
           //std::cout << "Fitness after CalcRun: " << it.fitness << std::endl;
           naturalizedTravelers.push_back(it);
           //std::cout << "The target run is: " << this->getRunNumber() <<std::endl;
           //it.getLedgers(0)->outputTrades();
           this->test->progsEvaled++;
       }
*/
       this->addTravelers(tempTravelers);
       Run::mtx.unlock();
       stats(this->test->gens);
   }
    
    //this->test->getEngine()->demeIsDone(this->getRunNumber());

    //this->test->currentBestFitness = -11111111111.0;

    
    this->test->function->calcTestFitness(this->test->getEvaluator(), this->population.at(this->test->bestIndividualIndex));
    std::cout << "Pop Size= " << this->population.size() << std::endl;
    float trainingEquity = this->population.at(this->test->bestIndividualIndex).getLedgers(0)->getTotalReturn();
    float testingEquity = this->population.at(this->test->bestIndividualIndex).getLedgers(2)->getTotalReturn();
    std::cout << "Deme: " << this->getRunNumber() << " Total Training Return: " << trainingEquity << " Fitness: " <<  this->population.at(this->test->bestIndividualIndex).fitness << std::endl;
    std::cout << "Deme: " << this->getRunNumber() << " Total Testing Return: " << testingEquity << std::endl;
    for (int i = 0; i < this->population.size(); ++i) {
        this->test->function->calcTestFitness(this->test->getEvaluator(), this->population.at(i));
    }
    this->population.at(this->test->bestIndividualIndex).getLedgers(0)->outputTrades();
    std::sort(this->population.begin(), this->population.end(), sort_on_test);
    for (int i = 0; i < 5; ++i) {
        this->test->getEngine()->addIndividual(this->population.at(i));
    }

}

void Run::migrate() {
    //std::cout << "Migrate" <<std::endl;
    if (this->population.size() < this->test->populationSize * 0.5) {
        return;
    }
    int i = 0;
    while (i < 10) {
        int deme = this->test->nextInt(this->test->getEngine()->numDemes);
        if (deme == this->getRunNumber()) {
            continue;
        }
        ++i;
        //std::cout << "The target is done? " << this->test->getEngine()->getDemeStruct(i).is_done << std::endl;
        if (!this->test->getEngine()->getDemeStruct(deme).is_done) {
            if (this->population.size() > 0) {
                
                //std::cout << j << " of 4" << std::endl;
                float chance = this->test->nextDbl();
                //std::cout << "past random********" << std::endl;
                if (chance < 0.3) {
                    //std::cout << "made the chance" << std::endl;
                    int traveler = this->tournament();
                    //std::cout << "After Tourn traveler is: " << traveler <<std::endl;
                    this->test->getEngine()->enqueueIndividual(deme, this->population.at(traveler));
                    //std::cout << "After EnQueue size is: " << this->population.size() <<std::endl;
                    this->population.erase(this->population.begin() + traveler);
                    //std::cout << "After Erase size is: " << this->population.size() <<std::endl;
                }
            }
        }
    }
    //std::cout << "After Migrate" <<std::endl;
}

void Run::addTravelers(std::vector<Indi> travelers) {
    //std::cout << "Add Travelers" <<std::endl;
    for (Indi it : travelers) {
        this->test->function->calcFitness(this->test->getEvaluator(), it, this->test->getDeme());
        this->test->function->calcValFitness(this->test->getEvaluator(), it, this->test->getDeme());
        this->test->progsEvaled++;
        this->population.push_back(it);
        //std::cout << "Added Traveler" <<std::endl;
        //std::cout << "After Adding size is: " << this->population.size() <<std::endl;
    }
    //std::cout << "done adding" <<std::endl;
}



void Run::printResults(int best){

}




