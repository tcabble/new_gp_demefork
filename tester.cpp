//
//  tester.cpp
//  
//
//  Created by Tim Cabble on 10/27/15.
//
//

#include <stdio.h>
#include <time.h>
#include <thread>
#include <mutex>
#include <random>
#include <chrono>
#include "Run.hpp"
#include "GPTest.hpp"
#include "Engine.hpp"
#include "Evaluator.hpp"
#include "Trade.hpp"
#include "TradeManager.hpp"




void launch(ChartData dataStore, Engine *theEngine, int seed, int index);

void testThread(int index);

int main(int argc, char *argv[]){
    ChartData dataStore;
    char *filename = argv[1];
    dataStore.loadData(filename);
    
    

    #define NUM_DEMES 4
    std::thread t[NUM_DEMES];
    
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, 36500);
    
    std::vector<std::vector<std::pair<int, int>> > ranges;
    
    for (int i = 0; i< 4; ++i) {
        std::vector<std::pair<int, int> > newPairVector;
        ranges.push_back(newPairVector);
    }
    ranges.at(0).emplace_back(std::make_pair(600, 550));
    ranges.at(0).emplace_back(std::make_pair(549, 500));
    ranges.at(1).emplace_back(std::make_pair(499, 450));
    ranges.at(1).emplace_back(std::make_pair(449, 400));
    ranges.at(2).emplace_back(std::make_pair(399, 350));
    ranges.at(2).emplace_back(std::make_pair(349, 300));
    ranges.at(3).emplace_back(std::make_pair(299, 250));
    ranges.at(3).emplace_back(std::make_pair(249, 200));
    

    Engine theEngine(NUM_DEMES);
    
    for (int i = 0; i < NUM_DEMES; ++i) {
        std::cout << "Demes setup" << std::endl;
        Deme theDeme;
        theDeme.is_done = false;
        theDeme.trainingRange = ranges.at(i).at(0);
        theDeme.validationRange = ranges.at(i).at(1);
        theEngine.addDemeStruct(theDeme);
        std::cout << "Size is " << theEngine.getDemeStructs().size() << std::endl;
        std::cout << "Pushed" << std::endl;
    /*
        theEngine.setGensWithout(i, 10);
        std::cout << "Set Something" << std::endl;
        theEngine.setPopulationSize(i, 30);
        theEngine.setMinLength(i, 3);
        theEngine.setMaxLength(i, 100);
        theEngine.setMaxDepth(i, 50);
        theEngine.setCrossoverProb(i, 0.95);
        theEngine.setTournSize(i, 2);
        theEngine.setTrainingRange(i, std::make_pair(8000, 7600));
        theEngine.setValidationRange(i, std::make_pair(7599, 7100));
        theEngine.setTestingRange(i, std::make_pair(7099, 0));
    */
    }
    
    std::cout << "Demes setup done" << std::endl;

    
    for (int i = 0; i < NUM_DEMES; ++i) {
        int seed = dis(gen);

        //t[i] = std::thread(testThread, i);
        t[i] = std::thread(launch, dataStore, &theEngine, seed, i);
    }

    for (int i = 0; i < NUM_DEMES; ++i) {
        t[i].join();
    }
    
    for (int i = 0; i < theEngine.getTravelers().size(); ++i) {
        for (int j = 0; j < theEngine.getTravelers().at(i).size(); ++j) {
            std::cout << "Someone lost at: " << i << ", " << j << std::endl;
        }
    }
    theEngine.sortIndividuals();
    theEngine.getIndividual(0).printIndividual();
    theEngine.printCode(0);
    theEngine.printResults();
    theEngine.getIndividual(0).getLedgers(2)->outputTrades();
    
    std::cout << "Done!" << std::endl;

    
}

void testThread(int index) {
    while (1) {
        std::stringstream stream;
        stream << "------------------- Thread: " << index << " -----------------";
        std::cout << stream.str() << std::endl;
    }
}

void launch(ChartData dataStore, Engine *theEngine, int seed, int index) {
    std::cout << "launching Thread" << std::endl;
    Deme theDeme = theEngine->getDemeStruct(index);
    
    Evaluator evaluator(&dataStore, 0);
    
    evaluator.addIndicator(0);
    evaluator.addIndicator(1);
    evaluator.addIndicator(2);
    evaluator.addIndicator(5);
    evaluator.addIndicator(3);
    
    FitnessFunction theFunction;
    
    GPTest theTest(theEngine, &theDeme, &theFunction, &evaluator, seed);
    for (int i = 0; i < 5; ++i) {
        theTest.gens = 0;
        
        Run newRun(&theTest, index);

        theEngine->addDemeTest(&theTest);

        newRun.createRandomPopulation();
        
        if(i == 0) {
            while(!theEngine->allDemesReady()) {
                std::this_thread::sleep_for (std::chrono::seconds(2));
            }
        }

        newRun.evolve();
    }
    theEngine->demeIsDone(index);
}