/*
 * Test.cpp
 *
 *  Created on: Feb 11, 2014
 *      Author: Tim
 *
 *  Implementation of the Test Class
 */

#include "GPTest.hpp"
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "Run.hpp"


//constructor - TODO: implement the constructor to work with inputs from a GUI
GPTest::GPTest(Engine *theEngine, Deme *theDeme, FitnessFunction *theFunction, Evaluator *eval, int seed):gen(seed), dis(0, 32600)
{

	this->engine = theEngine;
	this->function = theFunction;
	this->eval = eval;
	this->deme = theDeme;

	gen_wout_Imp = 20;
	populationSize = 100;
	maxLength = 100;
	minLength = 3;
	maxDepth = 50;
	permutation = 0.2;
	crossover_prob = 0.95;
	tourn_size = 2;
    currentBestFitness = -1111111111.0;

	this->progsEvaled = 0;
	this->gens = 0;

	//srand(time(0));
}

GPTest::~GPTest() {
	// TODO Auto-generated destructor stub
}


//Random number generation
int GPTest::nextInt(int edge){
	//int random_integer = rand() % edge;
	int random_integer = dis(gen) % edge;
	return random_integer;
}

float GPTest::nextDbl(){
	float dblnum=((nextInt(5000) + 1) / 5000.0);
	return (dblnum);
}

float GPTest::nextDbl(float fMin, float fMax){
	float f = (float)rand() / RAND_MAX;
	return fMin + f * (fMax - fMin);
}

Evaluator* GPTest::getEvaluator() {
    return this->eval;
}

Engine* GPTest::getEngine() {
    return this->engine;
}

Deme* GPTest::getDeme() {
	return this->deme;
}


//Helper to recursively parse and print the individual
int GPTest::printIndividual(std::string tempProgram, int bufferCnt){
/*	int a1 = 0,a2 = 0;
	if (tempProgram[bufferCnt] < this->FSET_START){//this->test->FSET_START){
		if ((int)tempProgram[bufferCnt] < this->varNumber){//this->test->varNumber){
			std::cout<< "X" << (int)tempProgram[bufferCnt++];
		}
		return(bufferCnt);
	}
	switch(tempProgram[bufferCnt]){
	   case 110:
		   std::cout << "(";
		   a1=printIndividual(tempProgram,++bufferCnt);
		   std::cout<<"+";
		   break;
	   case 111:
		   std::cout << "(";
		   a1=printIndividual(tempProgram,++bufferCnt);
		   std::cout<<"-";
		   break;
	   case 112:
		   std::cout << "(";
		   a1=printIndividual(tempProgram,++bufferCnt);
		   std::cout<<"*";
		   break;
	   case 113:
		   std::cout << "(";
		   a1=printIndividual(tempProgram,++bufferCnt);
		   std::cout<<"/";
		   break;
	   case 114:
		   std::cout << "(";
		   std::cout<<"MIN";
		   a1=printIndividual(tempProgram,++bufferCnt);
		   break;
	   case 115:
		   std::cout << "(";
		   std::cout<<"MAX";
		   a1=printIndividual(tempProgram,++bufferCnt);
		   break;
		case 116:
		   std::cout << "(";
		   std::cout<<"LESS";
		   a1=printIndividual(tempProgram,++bufferCnt);
		   break;
	   case 117:
		   std::cout << "(";
		   std::cout<<"GREAT";
		   a1=printIndividual(tempProgram,++bufferCnt);
		   break;
		case 118:
		   std::cout << "(";
		   std::cout<<"EQU";
		   a1=printIndividual(tempProgram,++bufferCnt);
		   break;
		case 119:
		   std::cout << "(";
		   std::cout<<"NEG";
		   a1=printIndividual(tempProgram,++bufferCnt);
		   break;
	   case 120:
		   std::cout << "(";
		   std::cout<<"SIN";
		   a1=printIndividual(tempProgram,++bufferCnt);
		   break;
	   case 121:
		   std::cout << "(";
		   std::cout<<"COS";
		   a1=printIndividual(tempProgram,++bufferCnt);
		   break;
	}
   if(tempProgram[bufferCnt-1] < this->FSET_END-2) {
	  a2=printIndividual(tempProgram,a1);
	  std::cout<<")";
	  return(a2);
   } else {
	   std::cout<<")";
	  return(a1);
   }
 */
    return 1;
}

void GPTest::printInputs(){
/*
	for (int i = 0; i < this->inputs->size(); i++){
		std::cout << "we're at: " << i << " - ";
		for (int j = 0; j < this->inputs->at(i).size(); j++){
			std::cout  << this->inputs->at(i).at(j) << " : ";
		}
		std::cout << std::endl;
	}
	std::cout << "DONE";

	for (std::vector<std::vector<float> >::iterator i = this->inputs->begin(); i != this->inputs->end(); i++){
		for (std::vector<float>::iterator j = this->inputs->at(i)->begin(); j != this->inputs->at(i)->end(); j++){
			std::cout << this->inputs->at(i)->at(j) << " : ";
		}
		std::cout << std::endl;
	}
*/

}

