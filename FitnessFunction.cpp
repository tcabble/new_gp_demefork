/*
 * FitnessFunction.cpp
 *
 *  Created on: Feb 17, 2014
 *      Author: Tim
 */

#include "FitnessFunction.hpp"
#include <string>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <memory>
#include <utility>

//constructor
FitnessFunction::FitnessFunction() {

}

FitnessFunction::~FitnessFunction() {

}

void FitnessFunction::calcFitness(Evaluator *eval, Indi &indiv, Deme* currentDeme) {
    //std::cout << "Calcing Fitness" << std::endl;
    //std::pair<int, int> range = eval->dataStore->getTrainingRange();
    std::pair<int, int> range = currentDeme->trainingRange;
    //std::cout << "Ranges in fitness: " << range.first << ":" << range.second << std::endl;
    indiv.getLedgers(0)->clearTrades();
    TradeManager thisManager(eval, &indiv , range.first, range.second);
    thisManager.runTrades(0);
    float totalReturn = indiv.getLedgers(0)->getTotalReturn();
    float maxDrawdown = indiv.getLedgers(0)->getMaxDrawdown();
    
    //indiv.fitness = indiv.getLedgers(0)->getTotalReturn();
    if (indiv.getLedgers(0)->tradeRegister.size() < 25 || indiv.getLedgers(0)->getTradeDistribution() < 0.15 || maxDrawdown == 0) {
        indiv.fitness = -11111111111111.0;
        indiv.equity = totalReturn;
    } else {
        //indiv.fitness = (indiv.getLedgers(0)->getTotalReturn() / indiv.getLedgers(0)->getMaxDrawdown() - (2 * indiv.getLedgers(0)->getStdDev()) - 
        //5.54 * indiv.program.size())  * indiv.getLedgers(0)->getWinLossRatio();
        //std::cout << "Drawdown: " << indiv.getLedgers(0)->getMaxDrawdown() << std::endl;
        //std::cout << "Relative Drawdown: " << indiv.getLedgers(0)->getMaxRelativeDrawdown() << std::endl;
        //indiv.fitness = indiv.getLedgers(0)->getEGM() * indiv.getLedgers(0)->getExpectedValue() - 5.54 * indiv.program.size();;
        
        if (maxDrawdown > 0) {
            indiv.equity = totalReturn;
            indiv.fitness = totalReturn / maxDrawdown - 1.54 * indiv.program.size();
            //indiv.fitness = indiv.getLedgers(0)->getTotalReturn() / indiv.getLedgers(0)->getMaxDrawdown() * 50  * 
            //((1 + indiv.getLedgers(0)->getExpectedValue()) * indiv.getLedgers(0)->getEGM()) - 18.54 * indiv.program.size();
        } else {
            //indiv.fitness = indiv.getLedgers(0)->getTotalReturn() / 0.0001 - 5.54 * indiv.program.size();
        }
        
    }
    //std::cout << "Fitness in calc after trade manager: " << indiv.fitness << std::endl;
}
    

void FitnessFunction::calcValFitness(Evaluator *eval, Indi &indiv, Deme* currentDeme) {
    //std::cout << "Calcing Val Fitness" << std::endl;
    //std::pair<int, int> range = eval->dataStore->getValidationRange();
    std::pair<int, int> range = currentDeme->validationRange;

    indiv.getLedgers(1)->clearTrades();
    TradeManager thisManager(eval, &indiv, range.first, range.second);
    thisManager.runTrades(1);
    
    float totalReturn = indiv.getLedgers(1)->getTotalReturn();
    float maxDrawdown = indiv.getLedgers(1)->getMaxDrawdown();
    if (totalReturn < 0) {
    	indiv.fitness = -11111111111111.0;
    	indiv.valFitness = -11111111111111.0;
        indiv.valEquity = totalReturn;
    } else {
        //indiv.valFitness = (indiv.getLedgers(1)->getTotalReturn() / indiv.getLedgers(1)->getMaxDrawdown() - (2 * indiv.getLedgers(1)->getStdDev()) - 
        //5.54 * indiv.program.size()) * indiv.getLedgers(1)->getWinLossRatio();
    	//indiv.valFitness = indiv.getLedgers(1)->getEGM() * indiv.getLedgers(1)->getExpectedValue() - 5.54 * indiv.program.size();;
        
        if (maxDrawdown > 0) {
            indiv.valFitness = totalReturn / maxDrawdown  - 1.54 * indiv.program.size();
            indiv.valEquity = totalReturn;
            //indiv.valFitness = indiv.getLedgers(1)->getTotalReturn() / indiv.getLedgers(1)->getMaxDrawdown() * 50  *
            //(1 + indiv.getLedgers(1)->getEGM()) - 18.54 * indiv.program.size();
        } else {
            //indiv.valFitness = indiv.getLedgers(1)->getTotalReturn() / 0.0001 - 5.54 * indiv.program.size();
        }
        
    }    
}

void FitnessFunction::calcTestFitness(Evaluator *eval, Indi &indiv) {
    //std::cout << "Calcing Val Fitness" << std::endl;
    std::pair<int, int> range = eval->dataStore->getTestingRange();
    indiv.getLedgers(2)->clearTrades();
    TradeManager thisManager(eval, &indiv, range.first, range.second);
    thisManager.runTrades(2);
    float totalReturn = indiv.getLedgers(2)->getTotalReturn();
    float maxDrawdown = indiv.getLedgers(2)->getMaxDrawdown();
    indiv.testReturn = totalReturn;
    indiv.testFitness = totalReturn / maxDrawdown  - 1.54 * indiv.program.size();   
}







