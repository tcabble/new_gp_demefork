//
//  Evaluator.hpp
//  
//
//  Created by Tim Cabble on 10/26/15.
//
//

#ifndef Evaluator_hpp
#define Evaluator_hpp

#include <stdio.h>
#include <cmath>
#include <memory>

#include "ChartData.hpp"
#include "Indi.hpp"
#include "Trade.hpp"
#include "ta_libc.h"

class Indi;

class Evaluator {
    
public:
    Evaluator(ChartData *historyData, int currentIndex);
    virtual ~Evaluator();

    ChartData *dataStore;

    int PC;

    int currentDataIndex;
    
    float run();
    
    void test();
    
    void reset(int index, Indi *indiv, Trade *newTrade, int mode);
    
    void setCurrentIndex(int index);
    
    void step();
    
    Bar getCurrentBar();
    
    Bar getBarAt(int index);

    void setDealSize(int mode);
    
    void addIndicator(int masterIndex);

    //Indi* getIndividual();

    //void setIndividual(Indi *indiv);
    std::function<float(int&, int&, int&, int&, int&)> getIndicator(int index);
    
    int getIndicatorIndex(int index);
    
    float ta_libTest(int &period, int &shift, int &three, int &four, int &five);
    
private:
    
    

    Indi *indiv;
    
    Trade *workingTrade;
    
    std::vector<int> indicatorIndex;
    
    //int currentDataIndex;
    
    //int PC;
    
    float sma(int &one, int &two, int &three, int &shift, int &opt_line);
    
    float macd(int &one, int &two, int &three, int &shift, int &opt_line);
    
    float stoch(int &one, int &two, int &three, int &shift, int &opt_line);
    
    float rsi(int &one, int &two, int &three, int &shift, int &opt_line);
    
    float mom(int &one, int &two, int &three, int &shift, int &opt_line);
    
    float kama(int &one, int &two, int &three, int &shift, int &opt_line);
    
    float roc(int &one, int &two, int &three, int &shift, int &opt_line);
    
    float open(int index);
    
    float high(int index);
    
    float low(int index);
    
    float close(int index);
    
    float volume(int index);


    
    
};

#endif /* Evaluator_hpp */
